package com.atlassian.connect.spring;

/**
 * {@code AddonAuthenticationType} specifies the authentication scheme that the app uses to communicate with Atlassian products for the
 * host site.
 * <em> IMPORTANT: Do not change the order of the enum values. The ordinals are stored in the database as integer.
 * If adding new enum values, always add to the bottom of the current list
 * </em>
 *
 * @see <a href="https://developer.atlassian.com/cloud/jira/platform/understanding-jwt-for-connect-apps/">JWT</a> default authentication scheme for Connect and Connect-on-Forge apps.
 */
public enum AddonAuthenticationType {

    /**
     * Default JWT authentication scheme for connect apps
     */
    JWT
}
