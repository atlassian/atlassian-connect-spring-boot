package com.atlassian.connect.spring;

import java.util.Optional;

/**
 * A service for retrieving the current Forge API context from the security context.
 * This will be populated for requests coming from Forge.
 * @see ForgeRemote
 * @since 5.1.0
 */
public interface ForgeContextRetriever {
    /**
     * Retrieves the current Forge API context from the security context.
     */
    Optional<ForgeApiContext> getForgeApiContext();
}
