package com.atlassian.connect.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation used on <a href="https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc">Spring Web MVC</a>
 * {@link org.springframework.stereotype.Controller}s or individual controller methods to denote that a context JWT is
 * acceptable on this endpoint. Such JWTs will contain the query string hash claim but it will not be a valid value
 *
 * <p>To accept requests using a context JWT authentication, add this annotation to the class
 * or method:
 * <blockquote><pre><code> &#64;Controller
 * &#64;ContextJWT
 * public class PublicController {
 *     ...
 * }</code></pre></blockquote>
 *
 * @since 2.1.3
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContextJwt {}
