package com.atlassian.connect.spring;

/**
 * Information about the Environment from which the Forge app is making the request
 */
public class ForgeEnvironment {
    private String type;
    private String id;
}
