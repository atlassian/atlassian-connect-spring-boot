package com.atlassian.connect.spring;

import org.springframework.data.annotation.Id;

import java.util.Objects;

/**
 * An Atlassian host mapping in which an installation id is mapped to its associated clientKey.
 * Mappings are stored in {@link AtlassianHostMappingRepository}.
 *
 * @see AtlassianHostMappingRepository
 */
public class AtlassianHostMapping {

    @Id
    private String installationId;              // "ari:cloud:ecosystem::installation/installation-identifier"

    private String clientKey;                   // "unique-client-identifier"

    /**
     * Creates a new empty installation client key (used by persistence mechanism).
     */
    public AtlassianHostMapping() {
    }

    /**
     * The string (in ARI format, e.g. ari:cloud:ecosystem::installation/c3658f0f-8380-41e5-bb1e-68903f8efdca)
     * identifying the installation of the app on Forge. Stable across upgrades but not uninstallation and
     * reinstallation. This value should be used to key Forge-related tenant details in your add-on.
     *
     * @return the installation ID
     */
    public String getInstallationId() {
        return installationId;
    }

    /**
     * Used by persistence mechanism.
     *
     * @param installationId the installation ID
     */
    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    /**
     * The identifying Connect key for the Atlassian product instance that the add-on was installed into.
     * This will never change for a given instance, and is unique across all Atlassian product tenants.
     * This value should be used to key Connect-related tenant details in your add-on.
     *
     * @return the client key of the host
     */
    public String getClientKey() {
        return clientKey;
    }

    /**
     * Used by persistence mechanism.
     *
     * @param clientKey the client key of the host
     */
    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtlassianHostMapping that = (AtlassianHostMapping) o;
        return Objects.equals(installationId, that.installationId) && Objects.equals(clientKey, that.clientKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(installationId, clientKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("AtlassianHostMapping{installationId= '%s', clientKey='%s'}",
                installationId, clientKey);
    }
}
