package com.atlassian.connect.spring;

/**
 * Information about the Forge app that is making the request
 */
public class ForgeApp {

    /**
     * The Forge App ID of the app that is making the request
     */
    private String id;

    /**
     * The Version of the Forge app that is making the request
     */
    private String version;

    /**
     * The Atlassian API Base URL to make requests to
     */
    private String apiBaseUrl;

    /**
     * The Installation ID from where the request originated
     */
    private String installationId;

    /**
     * Information about the Environment from which the Forge app is making the request
     */
    private ForgeEnvironment environment;

    /**
     * Information about the AppModule type making the request
     */
    private ForgeAppModule module;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    public String getInstallationId() {
        return installationId;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public ForgeEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(ForgeEnvironment environment) {
        this.environment = environment;
    }

    public ForgeAppModule getModule() {
        return module;
    }

    public void setModule(ForgeAppModule module) {
        this.module = module;
    }

    public ForgeApp() {
    }
}
