package com.atlassian.connect.spring.it.request;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.request.UserAgentProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.userAgent;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class UserAgentProviderIT {

    @Autowired
    private UserAgentProvider userAgentProvider;

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Value("${atlassian.connect.client-version}")
    private String atlassianConnectClientVersion;

    @Test
    void shouldReturnUserAgentFromContext() {
        assertThat(userAgentProvider.getUserAgent(),
                is(userAgent(atlassianConnectClientVersion, addonDescriptorLoader.getDescriptor().getKey())));
    }
}
