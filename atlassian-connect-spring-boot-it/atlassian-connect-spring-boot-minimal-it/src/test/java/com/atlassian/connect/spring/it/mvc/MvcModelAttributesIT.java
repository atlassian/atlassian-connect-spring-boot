package com.atlassian.connect.spring.it.mvc;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.IgnoreJwt;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.jwt.SymmetricJwtReader;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Collections;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class MvcModelAttributesIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    void shouldReturnAllJsModelAttributeForAuthenticated() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        String expectedUrl = "https://connect-cdn.atl-paas.net/all-debug.js";
        mvc.perform(get("/model")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectAllJsUrl']").value(is(expectedUrl)));
    }

    @Test
    void shouldReturnAllJsModelAttributeForAnonymous() throws Exception {
        mvc.perform(get("/model-public")
                        .param("xdm_e", "http://some-host.com")
                        .param("cp", "/context-path")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectAllJsUrl']").value(is("https://connect-cdn.atl-paas.net/all-debug.js")));
    }

    @Test
    void shouldReturnLicenseModelAttribute() throws Exception {
        String license = "none";
        mvc.perform(get("/model-public")
                        .param("lic", license)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectLicense']").value(is(license)));
    }

    @Test
    void shouldReturnPageJwtTokenModelAttributeForAuthenticated() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "cab9a26e-56ec-49e9-a08f-d7e4a19bde55";
        setJwtAuthenticatedPrincipal(host, subject);

        String addonKey = addonDescriptorLoader.getDescriptor().getKey();
        mvc.perform(get("/model")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectToken']").value(allOf(
                        validJwtWithClaim(host, "iss", addonKey),
                        validJwtWithClaim(host, "aud", Collections.singletonList(addonKey)),
                        validJwtWithClaim(host, "sub", subject),
                        validJwtWithClaim(host, SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()))));
    }

    @Test
    void shouldNotReturnPageJwtTokenModelAttributeForAnonymous() throws Exception {
        mvc.perform(get("/model-public")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectToken']").value(nullValue()));
    }

    private Matcher<String> validJwtWithClaim(final AtlassianHost host, final String claimName, final Object expected) {
        return new TypeSafeMatcher<>() {

            @Override
            protected boolean matchesSafely(String token) {
                return parseJwt(token).map(this::hasClaimValue).orElse(false);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.format("JWT having \"%s\" claim with value \"%s\"", claimName, expected));
            }

            @Override
            protected void describeMismatchSafely(String item, Description mismatchDescription) {
                String itemToDescribe = parseJwt(item).map(JWTClaimsSet::toString).orElse(String.format("Invalid JWT: %s", item));
                super.describeMismatchSafely(itemToDescribe, mismatchDescription);
            }

            private boolean hasClaimValue(JWTClaimsSet claims) {
                return Optional.ofNullable(claims.getClaim(claimName)).map((claimValue) -> claimValue.equals(expected)).orElse(false);
            }

            private Optional<JWTClaimsSet> parseJwt(String token) {
                try {
                    MACVerifier verifier = new MACVerifier(host.getSharedSecret());
                    return Optional.of(new SymmetricJwtReader(verifier).readAndVerify(token, null));
                } catch (JwtParseException | JwtVerificationException | JOSEException e) {
                    return Optional.empty();
                }
            }
        };
    }

    @TestConfiguration
    public static class ModelAttributesControllerConfiguration {

        @Bean
        public ModelAttributesController modelAttributesController() {
            return new ModelAttributesController();
        }
    }

    @Controller
    public static class ModelAttributesController {

        @IgnoreJwt
        @GetMapping(value = "/model-public")
        public ModelAndView getAnonymousModelAttributes(Model model) {
            return new ModelAndView(new MappingJackson2JsonView(), model.asMap());
        }

        @GetMapping(value = "/model")
        public ModelAndView getModelAttributes(Model model) {
            return getAnonymousModelAttributes(model);
        }
    }
}
