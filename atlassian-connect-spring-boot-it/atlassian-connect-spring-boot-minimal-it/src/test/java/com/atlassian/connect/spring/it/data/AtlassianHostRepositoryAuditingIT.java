package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class AtlassianHostRepositoryAuditingIT extends BaseApplicationIT {

    private static final String USER_KEY = "charlie";

    @BeforeEach
    public void setUp() {
        setJwtAuthenticatedPrincipal(new AtlassianHostBuilder().clientKey("some-client-key").build(), USER_KEY);
    }

    @Test
    void shouldStoreAuditingFields() {
        AtlassianHost host = new AtlassianHostBuilder()
                .clientKey("any-client-key")
                .build();
        hostRepository.save(host);

        AtlassianHost readHost = hostRepository.findById(host.getClientKey()).get();
        assertThat(readHost.getCreatedDate(), is(notNullValue()));
        assertThat(readHost.getCreatedBy(), equalTo(USER_KEY));
        assertThat(readHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(readHost.getLastModifiedBy(), equalTo(USER_KEY));
    }
}
