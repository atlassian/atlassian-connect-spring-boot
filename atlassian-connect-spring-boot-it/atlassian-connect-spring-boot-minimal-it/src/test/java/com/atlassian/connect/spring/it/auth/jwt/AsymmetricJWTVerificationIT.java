package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AsymmetricKeys;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.nimbusds.jose.JOSEException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.test.web.client.response.DefaultResponseCreator;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AsymmetricJWTVerificationIT extends BaseApplicationIT {

    private static final URI INSTALL_KEYS_URL_PRODUCTION = URI.create("https://asap-distribution.us-west-2.prod.atl-asap.net/");
    private static final URI INSTALL_KEYS_URL_STAGING = URI.create("https://asap-distribution.us-west-2.staging.atl-asap.net/");
    private static final URI INSTALL_KEYS_URL_FEDRAMP_PROD = URI.create("https://asap-distribution.us-west-2.prod.atl-auth-us-gov-mod.net/");
    private static final URI INSTALL_KEYS_URL_FEDRAMP_STAGING = URI.create("https://asap-distribution.us-west-2.staging.atl-auth-us-gov-mod.net/");
    private static final URI INSTALL_KEYS_FALLBACK_URL_PRODUCTION = URI.create("https://connect-install-keys.atlassian.com/");
    private static final URI TEST_INSTALL_KEYS_URL = URI.create("https://test-install-keys.atlassian.com/");

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    private AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void before() throws JOSEException {
        AsymmetricKeys.generateKeys(UUID.randomUUID().toString());
        hostRepository.deleteAll();
        // Reset public key base url in properties
        properties.setPublicKeyBaseUrl(null);
        RestTemplate restTemplate = asymmetricPublicKeyProvider.getAsymmetricPublicKeyProviderRestTemplate();
        mockServer = MockRestServiceServer.createServer(restTemplate);
        setMockServer(INSTALL_KEYS_URL_PRODUCTION);
    }

    @Test
    void shouldCreateHostFromInstalledEvent() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldParsePublicKeyWithDifferentLineBreakFormats() throws Exception {
        String publicKeyWin = AsymmetricKeys.getPublicKey().replace(System.lineSeparator(), "\r\n");
        setMockServer(INSTALL_KEYS_URL_PRODUCTION, AsymmetricKeys.getKeyId(), getResponseCreator(publicKeyWin));

        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldUseAsymmetricAuthenticationOnSecondInstall() throws JOSEException, JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldUninstall() throws JOSEException, JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        host = hostRepository.save(host);

        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String uninstalledJson = LifecycleBodyHelper.createLifecycleJson("uninstalled", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(uninstalledJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getUninstalledURI(), request, String.class);

        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(hostRepository.count(), is(1L));
        AtlassianHost updated = hostRepository.findAll().iterator().next();
        assertThat("Addon should not be installed", updated.isAddonInstalled(), is(false));
    }

    @Test
    void shouldRejectIfSignatureIsInvalidOnUninstall() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        AsymmetricKeys.generateKeys(AsymmetricKeys.getKeyId());
        setMockServer(INSTALL_KEYS_URL_PRODUCTION);

        String uninstalledJson = LifecycleBodyHelper.createLifecycleJson("uninstalled", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(uninstalledJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getUninstalledURI(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(0L));
    }

    @Test
    void shouldRejectSecondUnsignedInstall() throws JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new TestRestTemplate();
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    void shouldRejectIfAudienceDoesNotMatchBaseUrl() throws JsonProcessingException, JOSEException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String differentAppBaseUrl = "https://example.com";
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, differentAppBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(0L));
    }

    @Test
    void shouldRejectIfSignatureIsInvalid() throws JsonProcessingException, JOSEException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());

        AsymmetricKeys.generateKeys(AsymmetricKeys.getKeyId());
        setMockServer(INSTALL_KEYS_URL_PRODUCTION);

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(0L));
    }

    @Test
    void shouldFailIfKeyIdIsInvalid() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        String ramdonKeyId = UUID.randomUUID().toString();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), ramdonKeyId);
        setMockServer(INSTALL_KEYS_URL_PRODUCTION, ramdonKeyId, getResponseCreator(HttpStatus.NOT_FOUND));
        RequestMatcher req = requestTo(INSTALL_KEYS_FALLBACK_URL_PRODUCTION + ramdonKeyId);
        mockServer.expect(req).andRespond(getResponseCreator(HttpStatus.NOT_FOUND));

        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(0L));
    }

    @Test
    void shouldNotFallBackToSymmetricSignaturesOnFirstInstall() throws JsonProcessingException {
        String secret = "secret";
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        TestRestTemplate unsigned = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = unsigned.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(0L));
    }

    @Test
    void shouldNotFallBackToSymmetricSignaturesOnSecondInstallWithFallbackDisabled() throws JsonProcessingException {
        String clientKey = AtlassianHosts.CLIENT_KEY;
        String secret = "secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret";
        AtlassianHost host = new AtlassianHostBuilder().clientKey(clientKey).sharedSecret(secret).build();
        hostRepository.save(host);
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.empty());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldGetStagingPublicKeyForStagingTenants() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret, "https://example.jira-dev.com/wiki");
        setMockServer(INSTALL_KEYS_URL_STAGING);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldGetFedRAMPStagingPublicKeyForFedRAMPStagingTenants() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret, "https://example.atlassian-stg-fedm.net");
        setMockServer(INSTALL_KEYS_URL_FEDRAMP_STAGING);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldGetFedRAMPProdPublicKeyForFedRAMPProdTenants() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret, "https://example.atlassian-us-gov-mod.net");
        setMockServer(INSTALL_KEYS_URL_FEDRAMP_PROD);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    @Test
    void shouldUsePublicKeyServerDefinedInConfig() throws JOSEException, JsonProcessingException {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String secret = "secret";
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        String installedJson = LifecycleBodyHelper.createLifecycleJson("installed", secret);
        properties.setPublicKeyBaseUrl("https://test-install-keys.atlassian.com/");
        setMockServer(TEST_INSTALL_KEYS_URL);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(installedJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(getInstalledUri(), request, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(this.hostRepository.count(), is(1L));
        AtlassianHost insertedHost = this.hostRepository.findAll().iterator().next();
        assertThat(insertedHost.getClientKey(), is(host.getClientKey()));
    }

    private void setMockServer(URI publicKeyServer) throws JOSEException {
        DefaultResponseCreator resp = getResponseCreator(AsymmetricKeys.getPublicKey());
        setMockServer(publicKeyServer, AsymmetricKeys.getKeyId(), resp);
    }

    private void setMockServer(URI publicKeyServer, String keyId, DefaultResponseCreator resp) {
        RequestMatcher req = requestTo(publicKeyServer + keyId);
        mockServer.reset();
        mockServer.expect(req).andRespond(resp);
    }

    private DefaultResponseCreator getResponseCreator(HttpStatus status) {
        return withStatus(status);
    }

    private DefaultResponseCreator getResponseCreator(String publicKey) {
        return withSuccess(publicKey, MediaType.TEXT_PLAIN);
    }

    private URI getInstalledUri() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/installed").build().toUri();
    }

    private URI getUninstalledURI() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/uninstalled").build().toUri();
    }

}
