package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class RootRedirectControllerIT extends BaseApplicationIT {

    @Test
    void shouldRedirectRootToDescriptor() throws Exception {
        mvc.perform(get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/atlassian-connect.json"));
    }

    @Test
    void shouldRedirectRootToDescriptorWithContextPath() throws Exception {
        String contextPath = "/addon";
        mvc.perform(get(contextPath + "/")
                        .contextPath(contextPath)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl(contextPath + "/atlassian-connect.json"));
    }
}
