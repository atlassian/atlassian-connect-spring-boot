package com.atlassian.connect.spring.it.request.jwt;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;

import java.net.URI;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class JwtGeneratorIT extends BaseApplicationIT {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Test
    void shouldCreateJwtForRequestToStoredHostWithoutAuthenticatedHost() {
        createAndSaveHost(hostRepository);
        String jwt = atlassianHostRestClients.createJwt(HttpMethod.GET, URI.create(AtlassianHosts.BASE_URL + "/api"));
        assertThat(jwt, not(nullValue()));
    }
}
