package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.ForgeSystemAccessToken;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

@SpringBootTest
class ForgeSystemAccessTokenRepositoryIT extends BaseApplicationIT {

    // https://developer.atlassian.com/platform/forge/remote/essentials/#the-forge-invocation-token--fit-
    private static final String ACCESS_TOKEN = """
        eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcmk6Y2xvdWQ6ZWNvc3lzdGVtOjphcHAvOGRiMzM4MDktMWYzMi00OGJiLThjNTItNTg3N2RhYjQ4MTA3IiwiaXNzIjoiZm9yZ2UvaW52b2NhdGlvbi10b2tlbiIsImlhdCI6MTcwMDE3NTE0OSwibmJmIjoxNzAwMTc1MTQ5LCJleHAiOjE3MDAxNzUxNzQsImp0aSI6ImQ4YTQ5NjI1M2VjOGMxOGE1NDYzMWU0YzgyY2JlZGQ1ZDBhZTg1NzAifQ.Pr6AalO7yfaKTOuvgnxxbudLyN2WnVgim5Kxh3H5Kbk
    """.trim();
    private static final String INSTALLATION_ID = "ari:cloud:ecosystem::installation/0a3a7799-53ae-4a5b-9e7e-03338980abb";
    private static final String API_BASE_URL = "https://api.stg.atlassian.com/ex/confluence/d0d52620-3203-4cfa-8db5-f2587155f0dd";
    private static final long EXPIRATION_TIME = 1700175174L;
    private static final long LEEWAY_MS = 1000L;

    @Transactional
    @Test
    void shouldFetchUnexpiredToken() throws ParseException {
        ForgeSystemAccessToken entity = initForgeSystemAccessToken();
        forgeSystemAccessTokenRepository.save(entity);

        Instant beforeExpiration = Instant.ofEpochSecond(EXPIRATION_TIME).minusMillis(LEEWAY_MS);
        Optional<ForgeSystemAccessToken> optionalToken = forgeSystemAccessTokenRepository.findByInstallationIdAndExpirationTimeAfter(
            INSTALLATION_ID,
            Timestamp.from(beforeExpiration)
        );
        assertThat(optionalToken.isPresent(), equalTo(true));
        assertThat(optionalToken.get().getAccessToken(), equalTo(ACCESS_TOKEN));
        assertThat(optionalToken.get().getApiBaseUrl(), equalTo(API_BASE_URL));
    }

    @Transactional
    @Test
    void shouldNotFetchExpiredToken() throws ParseException {
        ForgeSystemAccessToken entity = initForgeSystemAccessToken();
        forgeSystemAccessTokenRepository.save(entity);

        Instant afterExpiration = Instant.ofEpochSecond(EXPIRATION_TIME).plusMillis(LEEWAY_MS);
        Optional<ForgeSystemAccessToken> optionalToken = forgeSystemAccessTokenRepository.findByInstallationIdAndExpirationTimeAfter(
            INSTALLATION_ID,
            Timestamp.from(afterExpiration)
        );
        assertThat(optionalToken.isEmpty(), equalTo(true));
    }

    @Transactional
    @Test
    void shouldCleanStaleRecordsOnly() throws ParseException {
        long affectedRows;
        Instant criteria;
        ForgeSystemAccessToken entity = initForgeSystemAccessToken();

        // deleting stale records
        forgeSystemAccessTokenRepository.save(entity);
        criteria = Instant.ofEpochSecond(EXPIRATION_TIME).plusMillis(LEEWAY_MS);
        affectedRows = forgeSystemAccessTokenRepository.deleteAllByExpirationTimeBefore(
            Timestamp.from(criteria)
        );
        assertThat(affectedRows, greaterThanOrEqualTo(1L));

        // not deleting fresh records
        forgeSystemAccessTokenRepository.save(entity);
        criteria = Instant.ofEpochSecond(EXPIRATION_TIME).minusMillis(LEEWAY_MS);
        affectedRows = forgeSystemAccessTokenRepository.deleteAllByExpirationTimeBefore(
            Timestamp.from(criteria)
        );
        assertThat(affectedRows, lessThanOrEqualTo(0L));
    }

    private static ForgeSystemAccessToken initForgeSystemAccessToken() throws ParseException {
        ForgeSystemAccessToken entity = new ForgeSystemAccessToken();
        entity.setInstallationId(INSTALLATION_ID);
        entity.setApiBaseUrl(API_BASE_URL);
        entity.setAccessToken(ACCESS_TOKEN);
        entity.parseAndSetClaims(ACCESS_TOKEN);
        return entity;
    }
}
