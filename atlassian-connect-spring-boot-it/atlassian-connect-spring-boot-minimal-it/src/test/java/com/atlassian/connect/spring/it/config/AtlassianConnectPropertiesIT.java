package com.atlassian.connect.spring.it.config;

import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(value = AtlassianConnectProperties.class)
class AtlassianConnectPropertiesIT {

    // Use @Nested to simulate @TestPropertySource at method level.
    @Nested
    @TestPropertySource(properties={ "atlassian.connect.all-js-url=https://test.com", "atlassian.connect.debug-all-js=true" })
    class CustomAllJsUrlTest {
        @Autowired
        private AtlassianConnectProperties properties;

        @Test
        void shouldReturnCustomAllJsUrl() {
            assertThat(properties.getAllJsUrl(), is("https://test.com"));
        }
    }

    @Nested
    @TestPropertySource(properties={ "atlassian.connect.debug-all-js = false" })
    class AllJsUrlTest {
        @Autowired
        private AtlassianConnectProperties properties;

        @Test
        void shouldReturnDefaultAllJsUrl() {
            assertThat(properties.getAllJsUrl(), is(AtlassianConnectProperties.DEFAULT_ALL_JS_URL));
        }
    }

    @Nested
    @TestPropertySource(properties={ "atlassian.connect.debug-all-js=true" })
    class DebugAllJsUrlTest {
        @Autowired
        private AtlassianConnectProperties properties;

        @Test
        void shouldReturnDefaultAllDebugJsUrl() {
            assertThat(properties.getAllJsUrl(), is(AtlassianConnectProperties.DEFAULT_ALL_DEBUG_JS_URL));
        }
    }
}
