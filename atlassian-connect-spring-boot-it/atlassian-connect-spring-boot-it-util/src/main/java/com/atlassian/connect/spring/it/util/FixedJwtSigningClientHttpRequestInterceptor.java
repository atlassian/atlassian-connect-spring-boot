package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Optional;

public class FixedJwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private final String jwt;

    @Autowired
    private AtlassianHostUriResolver hostUriResolver;


    public FixedJwtSigningClientHttpRequestInterceptor(String jwt) {
        super(() -> "some-user-agent");
        this.jwt = jwt;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(AtlassianHosts.BASE_URL);
        return Optional.of(host);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", jwt));
        return request;
    }

    @Override
    protected URI wrapUri(HttpRequest request, AtlassianHost host) {
        URI uri = request.getURI();
        return uri.isAbsolute() ? uri : hostUriResolver.resolveToAbsoluteUriWithBase(uri, host.getBaseUrl());
    }
}
