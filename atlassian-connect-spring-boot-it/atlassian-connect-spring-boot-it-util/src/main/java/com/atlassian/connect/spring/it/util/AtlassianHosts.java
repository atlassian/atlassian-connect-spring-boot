package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;

public final class AtlassianHosts {

    public static final String CLIENT_KEY = "some-host-client-key";
    public static final String PRODUCT_TYPE = "some-product";

    public static final String SHARED_SECRET = "11111111111111111111111111111111"; // must be 32 characters long
    public static final String OTHER_SHARED_SECRET = "22222222222222222222222222222222";

    public static final String BASE_URL = "http://example.com/product";
    public static final String OAUTH_CLIENT_ID = "some-client-id";
    public static final String CLOUD_ID = "some-cloud-id";
    public static final String INSTALLATION_ID = "ari-installation-id";

    private AtlassianHosts() {}

    public static AtlassianHost createAndSaveHost(AtlassianHostRepository hostRepository) {
        AtlassianHost host = new AtlassianHostBuilder().build();
        hostRepository.save(host);
        return host;
    }

    public static AtlassianHost createAndSaveHostWithAuthenticationAndCloudId(AtlassianHostRepository hostRepository, AddonAuthenticationType authenticationType) {
        AtlassianHost host = new AtlassianHostBuilder().build();
        host.setCloudId(CLOUD_ID);
        host.setAuthentication(authenticationType);
        hostRepository.save(host);
        return host;
    }
}
