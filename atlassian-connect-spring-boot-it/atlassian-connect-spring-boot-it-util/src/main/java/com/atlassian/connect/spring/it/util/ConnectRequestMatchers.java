package com.atlassian.connect.spring.it.util;

import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.system.JavaVersion;
import org.springframework.core.SpringVersion;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.client.RequestMatcher;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;

public class ConnectRequestMatchers {

    private ConnectRequestMatchers() {}

    public static RequestMatcher bearerAuthorizationHeader(String token) {
        return header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
    }

    public static RequestMatcher noAuthorizationHeader() {
        return request -> assertThat(request.getHeaders(), not(hasKey(HttpHeaders.AUTHORIZATION)));
    }

    public static RequestMatcher onlyOneAuthorizationHeader() {
        return request -> {
            List<String> values = request.getHeaders().get(HttpHeaders.AUTHORIZATION);
            assertThat("Authorization header", values, hasSize(1));
        };
    }

    public static RequestMatcher userAgentHeader(String atlassianConnectClientVersion, String appKey) {
        assertThat(atlassianConnectClientVersion, is(notNullValue()));
        return header(HttpHeaders.USER_AGENT, userAgent(atlassianConnectClientVersion, appKey));
    }

    public static String userAgent(String atlassianConnectClientVersion, String appKey) {
        return String.format("%s/%s %s/%s %s/%s %s/%s %s",
                "atlassian-connect-spring-boot", atlassianConnectClientVersion,
                "SpringBoot", SpringBootVersion.getVersion(),
                "Spring", SpringVersion.getVersion(),
                "Java", JavaVersion.getJavaVersion().toString(),
                appKey);
    }
}
