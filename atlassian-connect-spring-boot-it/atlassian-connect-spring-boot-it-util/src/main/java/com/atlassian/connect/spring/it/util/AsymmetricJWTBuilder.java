package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.internal.jwt.JwtSigningException;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;

import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;

public class AsymmetricJWTBuilder extends JwtBuilder {

    private PrivateKey privateKey;
    private String keyId;

    @Override
    public AsymmetricJWTBuilder issuer(String iss) {
        jwtJsonBuilder.issuer(iss);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder subject(String sub) {
        jwtJsonBuilder.subject(sub);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder audience(String aud) {
        jwtJsonBuilder.audience(aud);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder expirationTime(long exp) {
        jwtJsonBuilder.expirationTime(exp);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder notBefore(long nbf) {
        jwtJsonBuilder.notBefore(nbf);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder issuedAt(long iat) {
        jwtJsonBuilder.issuedAt(iat);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder queryHash(String queryHash) {
        jwtJsonBuilder.queryHash(queryHash);
        return this;
    }

    @Override
    public AsymmetricJWTBuilder claim(String name, Object value) {
        jwtJsonBuilder.claim(name, value);
        return this;
    }

    public AsymmetricJWTBuilder asymmetricSignature(RSAPrivateKey privateKey, String keyId) {
        this.privateKey = privateKey;
        this.keyId = keyId;
        return this;
    }

    @Override
    public String build() {
        String jwtPayload = jwtJsonBuilder.build();
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(keyId).build();
        JWSObject jwsObject = new JWSObject(header, new Payload(jwtPayload));

        RSASSASigner signer = new RSASSASigner(this.privateKey);
        try {
            jwsObject.sign(signer);
        } catch (JOSEException e) {
            throw new JwtSigningException(e);
        }
        return jwsObject.serialize();
    }
}
