package com.atlassian.connect.spring.it.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LifecycleBodyHelper {

    private LifecycleBodyHelper() {}

    public static String createLifecycleJson(String eventType, String secret) throws JsonProcessingException {
        return createLifecycleJson(eventType, secret, AtlassianHosts.BASE_URL);
    }

    public static Map<String, Object> createLifecycleEventMap(String eventType) {
        return new LifecycleEventBuilder(eventType, AtlassianHosts.BASE_URL).build();
    }

    public static String createLifecycleJson(String eventType, String secret, String baseUrl) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(new LifecycleEventBuilder(eventType, baseUrl).secret(secret).build());
    }

    public static String createHarmonizedLifecycleJson(String eventType, String installationId) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(new LifecycleEventBuilder(eventType, AtlassianHosts.BASE_URL).installationId(installationId).build());
    }

    public static String createLifecycleJsonWithCapabilitySet(String eventType, String capabilitySet) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(new LifecycleEventBuilder(eventType, AtlassianHosts.BASE_URL).capabilitySet(capabilitySet).build());
    }

    private static class LifecycleEventBuilder {
        Map<String, Object> eventMap = new HashMap<>();

        public LifecycleEventBuilder(String eventType, String baseUrl) {
            eventMap.put("key", "atlassian-connect-spring-boot-test");
            eventMap.put("clientKey", AtlassianHosts.CLIENT_KEY);
            eventMap.put("serverVersion", "server-version");
            eventMap.put("pluginsVersion", "version-of-connect");
            eventMap.put("baseUrl", baseUrl);
            eventMap.put("displayUrl", AtlassianHosts.BASE_URL);
            eventMap.put("displayUrlServicedeskHelpCenter", AtlassianHosts.BASE_URL);
            eventMap.put("productType", AtlassianHosts.PRODUCT_TYPE);
            eventMap.put("description", "Atlassian JIRA at https://example.atlassian.net");
            eventMap.put("serviceEntitlementNumber", "SEN-number");
            eventMap.put("entitlementId", "entitlement-id");
            eventMap.put("entitlementNumber", "entitlement-number");
            eventMap.put("eventType", eventType);
            if (eventType.equals("installed")) {
                eventMap.put("sharedSecret", AtlassianHosts.SHARED_SECRET);
            }
            // add a field which is not part of the lifecycle event to ensure these do not trip up reception
            eventMap.put("installSong", "NeverGonnaGiveYouUp");
        }

        public LifecycleEventBuilder installationId(String installationId) {
            if (installationId != null) {
                eventMap.put("installationId", installationId);
            }
            return this;
        }

        public LifecycleEventBuilder capabilitySet(String capabilitySet) {
            if (capabilitySet != null) {
                eventMap.put("capabilitySet", capabilitySet);
            }
            return this;
        }

        public LifecycleEventBuilder secret(String secret) {
            if (secret != null && this.eventMap.get("eventType").equals("installed")) {
                eventMap.put("sharedSecret", secret);
            }
            return this;
        }

        public Map<String, Object> build() {
            return Collections.unmodifiableMap(eventMap);
        }
    }
}
