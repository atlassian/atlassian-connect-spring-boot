package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.jwt.JwtBuilder;
import com.atlassian.connect.spring.internal.request.jwt.JwtQueryHashGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

public class AtlassianHostJwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private final String clientKey;
    private final String sharedSecret;
    private final Optional<String> optionalSubject;
    private final JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();
    private final Optional<Map<String, Object>> optionalContext;

    @Autowired
    private AtlassianHostUriResolver hostUriResolver;

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(AtlassianHost host,
                                                               Optional<String> optionalSubject) {
        this(host.getClientKey(), host.getSharedSecret(), optionalSubject);
    }

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(AtlassianHost host,
                                                               Optional<String> optionalSubject,
                                                               Optional<Map<String, Object>> optionalContext) {
        this(host.getClientKey(), host.getSharedSecret(), optionalSubject, optionalContext);
    }

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(String clientKey, String sharedSecret,
                                                               Optional<String> optionalSubject) {
        this(clientKey, sharedSecret, optionalSubject, Optional.empty());
    }

    public AtlassianHostJwtSigningClientHttpRequestInterceptor(String clientKey, String sharedSecret,
                                                               Optional<String> optionalSubject,
                                                               Optional<Map<String, Object>> optionalContext) {
        super(() -> "some-user-agent");
        this.clientKey = clientKey;
        this.sharedSecret = sharedSecret;
        this.optionalSubject = optionalSubject;
        this.optionalContext = optionalContext;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(AtlassianHosts.BASE_URL);
        return Optional.of(host);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", createJwt(request.getMethod(), request.getURI())));
        return request;
    }

    @Override
    protected URI wrapUri(HttpRequest request, AtlassianHost host) {
        URI uri = request.getURI();
        return uri.isAbsolute() ? uri : hostUriResolver.resolveToAbsoluteUriWithBase(uri, host.getBaseUrl());
    }

    public String createJwt(HttpMethod method, URI uri) {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(
                method, uri, AtlassianHostUriResolver.getBaseUrl(uri));
        String queryHash = queryHashGenerator.computeCanonicalRequestHash(canonicalHttpRequest);
        JwtBuilder jwtBuilder = new JwtBuilder()
                .issuer(clientKey)
                .queryHash(queryHash)
                .signature(sharedSecret);
        optionalSubject.ifPresent(jwtBuilder::subject);
        optionalContext.ifPresent(context -> jwtBuilder.claim("context", context));
        return jwtBuilder.build();
    }
}
