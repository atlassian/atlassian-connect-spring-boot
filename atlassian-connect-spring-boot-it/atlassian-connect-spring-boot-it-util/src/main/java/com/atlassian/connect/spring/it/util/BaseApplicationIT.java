package com.atlassian.connect.spring.it.util;


import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMappingRepository;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.AtlassianHostUser.AtlassianHostUserBuilder;
import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.test.context.TestSecurityContextHolder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class BaseApplicationIT {

    protected MockMvc mvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected AtlassianHostRepository hostRepository;

    @Autowired
    protected AtlassianHostMappingRepository hostMappingRepository;


    @Autowired
    protected ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository;

    @Autowired
    protected ConfigurableEnvironment environment;

    @BeforeEach
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @AfterEach
    public void tearDown() {
        TestSecurityContextHolder.clearContext();
        hostRepository.deleteAll();
        hostMappingRepository.deleteAll();
        forgeSystemAccessTokenRepository.deleteAll();
    }

    protected String getServerAddress() {
        String port = environment.getProperty("local.server.port", "8080");
        return "http://localhost:" + port;
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host) {
        setAuthenticatedPrincipal(host, null);
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host, String userAccountId) {
        setAuthenticatedPrincipal(host, userAccountId);
    }

    protected void setOAuth2AuthenticatedPrincipal(AtlassianHost host, String userAccountId) {
        setAuthenticatedPrincipal(host, userAccountId);
    }

    protected void setAuthenticatedPrincipal(AtlassianHost host, String userAccountId) {
        AtlassianHostUserBuilder hostUserBuilder = AtlassianHostUser.builder(host);
        if (userAccountId != null) {
            hostUserBuilder.withUserAccountId(userAccountId);
        }
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(hostUserBuilder.build(), null);
        authentication.setAuthenticated(true);
        TestSecurityContextHolder.setAuthentication(authentication);
    }
}
