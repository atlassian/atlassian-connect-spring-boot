package com.atlassian.connect.spring.it.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(OutputCaptureExtension.class)
class NoHostRepositoryIT {

    @Test
    void shouldPrintFailureAnalysisWhenMissingAtlassianHostRepository(CapturedOutput capturedOutput) {
        try {
            new SpringApplication(NoDataAddonApplication.class).run();
        } catch (Exception e) {
            // We are inspecting the log instead
        }

        String log = capturedOutput.toString();
        assertThat(log, allOf(containsString("APPLICATION FAILED TO START"),
                containsString("Choose a Spring Data implementation to use with AtlassianHostRepository")));
    }
}
