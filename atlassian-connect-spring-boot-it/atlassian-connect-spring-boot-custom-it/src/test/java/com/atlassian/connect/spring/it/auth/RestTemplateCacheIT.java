package com.atlassian.connect.spring.it.auth;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.client.RestTemplate;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RestTemplateCacheIT extends BaseApplicationIT {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @WithMockUser
    @Test
    void shouldRetrieveOAuth2ClientWithCaching() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host).withUserAccountId("some-account-id").build();

        RestTemplate restTemplate = this.atlassianHostRestClients.authenticatedAs(hostUser);
        assertThat(restTemplate, allOf(is(notNullValue()), is(this.atlassianHostRestClients.authenticatedAs(hostUser))));
    }
}
