package com.atlassian.connect.spring.internal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AtlassianConnectContextModelAttributeProviderTest {
    
    @Mock
    private AtlassianConnectProperties atlassianConnectProperties;

    @InjectMocks
    private AtlassianConnectContextModelAttributeProvider modelAttributeProvider;

    @Test
    void shouldReturnCDNAllJs() {
        String allJsUrl = "https://connect-cdn.atl-paas.net/all.js";
        when(atlassianConnectProperties.getAllJsUrl()).thenReturn(allJsUrl);
        assertThat(modelAttributeProvider.getAllJsUrl(), is(allJsUrl));
    }
}
