package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidJwtException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.nimbusds.jwt.JWTClaimsSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class AsymmetricAuthenticationProviderTest {

    @Mock
    private UnaryOperator<String> publicKeyBaseURISupplier;
    @Mock
    private UnaryOperator<String> fallbackPublicKeyBaseURISupplier;
    @Mock
    private AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;
    @Mock
    private AtlassianConnectProperties atlassianConnectProperties;
    @Mock
    private AddonDescriptorLoader addonDescriptorLoader;
    @Mock
    private AtlassianHostRepository hostRepository;
    @Mock
    private JWTClaimsSet jwtClaimsSet;
    @Mock
    private AsymmetricCredentials credentials;
    @Mock
    private AddonDescriptor addonDescriptor;
    @Mock
    private CanonicalHttpRequest canonicalHttpRequest;

    private AsymmetricAuthenticationProvider asymmetricAuthenticationProvider;

    @BeforeEach
    public void setUp() {
        when(jwtClaimsSet.getAudience()).thenReturn(List.of("baseUrl"));
        when(addonDescriptor.getBaseUrls()).thenReturn(Stream.of(URI.create("nonMatchingBaseUrl")));
        when(addonDescriptorLoader.getDescriptor()).thenReturn(addonDescriptor);
        when(canonicalHttpRequest.getMethod()).thenReturn("POST");
        when(canonicalHttpRequest.getRelativePath()).thenReturn("fake");
        when(credentials.getCanonicalHttpRequest()).thenReturn(canonicalHttpRequest);
    }

    @Test
    void shouldThrowIfPropertiesDoesNotContainsAudClaimUrl() {
        asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, publicKeyBaseURISupplier, fallbackPublicKeyBaseURISupplier, asymmetricPublicKeyProvider, atlassianConnectProperties);
        assertThrows(InvalidJwtException.class, () -> asymmetricAuthenticationProvider.buildAsymmetricAuthentication(credentials, jwtClaimsSet));
    }

    @Test
    void shouldNotThrowIfPropertiesContainsAudClaimUrl() {
        when(atlassianConnectProperties.getAllowedBaseUrls()).thenReturn(List.of("baseUrl"));
        asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, publicKeyBaseURISupplier, fallbackPublicKeyBaseURISupplier, asymmetricPublicKeyProvider, atlassianConnectProperties);
        assertDoesNotThrow(() -> asymmetricAuthenticationProvider.buildAsymmetricAuthentication(credentials, jwtClaimsSet));
    }

    @Test
    void shouldNotThrowIfDescriptorBaseUrlsMatchesAudClaimUrl() {
        when(addonDescriptor.getBaseUrls()).thenReturn(Stream.of(URI.create("baseUrl")));
        asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, publicKeyBaseURISupplier, fallbackPublicKeyBaseURISupplier, asymmetricPublicKeyProvider, atlassianConnectProperties);
        assertDoesNotThrow(() -> asymmetricAuthenticationProvider.buildAsymmetricAuthentication(credentials, jwtClaimsSet));
    }

    @Test
    void shouldThrowIfGetAllowedBaseUrlsAreInvalid() {
        when(atlassianConnectProperties.getAllowedBaseUrls()).thenReturn(List.of("[]"));
        assertThrows(IllegalArgumentException.class, () -> asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, publicKeyBaseURISupplier, fallbackPublicKeyBaseURISupplier, asymmetricPublicKeyProvider, atlassianConnectProperties));
    }

    @Test
    void shouldThrowIfGetAllowedBaseUrlsIsNull() {
        when(atlassianConnectProperties.getAllowedBaseUrls()).thenReturn(null);
        assertThrows(NullPointerException.class, () -> asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, publicKeyBaseURISupplier, fallbackPublicKeyBaseURISupplier, asymmetricPublicKeyProvider, atlassianConnectProperties));
    }
}
