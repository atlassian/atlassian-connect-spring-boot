package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.connect.spring.internal.AtlassianConnectProperties.RequestUriEncoding.NONE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AtlassianRequestUtilsTest {

    private static final String BASE_URI = "https://example.atlassian.net";
    private static final String PATH = "/path";

    private final static String TEST = "test";
    private static final String ENCODED_TITLE_TEST_1 = "test%201"; // "test 1"
    private static final String ENCODED_TITLE_TEST_PLUS_1 = "test%2B1"; // "test+1"
    private static final String ENCODED_TITLE_TEST_AND_1 = "test%261"; // "test&1"
    private static final String ENCODED_TITLE_TEST_BRACKET = "t%5Bes%5Dt"; // "t[es]t"
    private static final String ENCODED_TITLE_TEST_HASH = "tes%23t"; // "tes#t"
    private static final String ENCODED_TITLE_COMPLEX_UTF8 = "%22SUPPORT-4509%20-%20%5Bform%5D%20-%20C%23%E5%8C%96%22"; // "SUPPORT-4509 - [form] - C#化"

    @ParameterizedTest
    @ValueSource(strings = {TEST, ENCODED_TITLE_TEST_1, ENCODED_TITLE_TEST_PLUS_1, ENCODED_TITLE_TEST_AND_1, ENCODED_TITLE_TEST_BRACKET, ENCODED_TITLE_TEST_HASH, ENCODED_TITLE_COMPLEX_UTF8})
    void testEncodedUriShouldNotBeChanged(String value) throws URISyntaxException {
        // set up
        AtlassianConnectProperties atlassianConnectProperties = new AtlassianConnectProperties();
        atlassianConnectProperties.setUriEncoding(NONE);
        AtlassianRequestUtils atlassianRequestUtils = new AtlassianRequestUtils(atlassianConnectProperties);
        URI baseUri = new URI(BASE_URI);
        URI uri = new URI(BASE_URI + PATH + getQuery(value));

        // action
        URI relativeUri = atlassianRequestUtils.getRelativeUriToResolve(uri, baseUri);

        // assert
        String relativeUriString = relativeUri.toString();
        assertEquals(relativeUriString, PATH + getQuery(value));
    }

    @Test
    void getRelativeUriToResolve_shouldPreserveOriginalPath_whenUriEncodingIsNone() throws URISyntaxException {
        // given
        AtlassianConnectProperties atlassianConnectProperties = new AtlassianConnectProperties();
        atlassianConnectProperties.setUriEncoding(NONE);
        AtlassianRequestUtils atlassianRequestUtils = new AtlassianRequestUtils(atlassianConnectProperties);
        URI baseURI = new URI(BASE_URI);
        // "/abc €áÁäÄàâÀÂ()_+*&$€¥¡!%@#¿?:,.-"
        String encodedPath = "/abc%20%E2%82%AC%C3%A1%C3%81%C3%A4%C3%84%C3%A0%C3%A2%C3%80%C3%82()_+*&$%E2%82%AC%C2%A5%C2%A1!%25@#%C2%BF?:,.-";
        URI encodedPathURI = new URI(encodedPath);

        // when
        URI relativeUri = atlassianRequestUtils.getRelativeUriToResolve(encodedPathURI, baseURI);

        // then
        assertThat(relativeUri).hasToString(encodedPath);
    }

    @Test
    void shouldThrowExceptionWhenAuthorizationGrantTypeMismatch() {
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            AtlassianRequestUtils.validateAuthorizationGrantType(AuthorizationGrantType.JWT_BEARER, AuthorizationGrantType.CLIENT_CREDENTIALS);
        });

        String expectedMessage = "Authorization grant type urn:ietf:params:oauth:grant-type:jwt-bearer does not match the desired type client_credentials, can not authorize.";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    private String getQuery(String value) {
        return "?param=" + value;
    }

}

