package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class JwtQueryHashGeneratorTest {

    public static Stream<?> signedUrlFixtures() throws IOException {
        URL resource = JwtQueryHashGeneratorTest.class.getResource("/jwt-signed-urls.json");
        SignedUrls signedUrls = new ObjectMapper().readValue(resource, SignedUrls.class);
        return signedUrls.tests.stream();
    }

    private static final String BASE_URL = "https://www.example.com";

    private static final HttpMethod REQUEST_METHOD = HttpMethod.GET;

    private final JwtQueryHashGenerator queryHashGenerator = new JwtQueryHashGenerator();

    @ParameterizedTest(name = "{index}: {0}")
    @MethodSource("signedUrlFixtures")
    void test(SignedUrlTest signedUrlTest) throws URISyntaxException {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(
                REQUEST_METHOD, new URI(signedUrlTest.signedUrl), BASE_URL);
        assertThat(HttpRequestCanonicalizer.canonicalize(canonicalHttpRequest), is(signedUrlTest.canonicalUrl));
    }

    private static class SignedUrls {

        @JsonProperty
        private String secret;

        @JsonProperty
        private List<SignedUrlTest> tests;

        @JsonProperty
        private String comment;
    }

    private static class SignedUrlTest {

        @JsonProperty
        private String name;

        @JsonProperty
        private String canonicalUrl;

        @JsonProperty
        private String signedUrl;

        @Override
        public String toString() {
            return name;
        }
    }
}
