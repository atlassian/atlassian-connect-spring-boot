package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(MockitoExtension.class)
class AtlassianHostUriResolverTest {

    private static final String PRODUCT_TYPE = "jira";
    private static final String CLOUD_ID = "acloudid";

    private static final String BASE_URI_STRING = "https://mysite.atlassian.net";
    private static final String BASE_URI_STG_STRING = "https://mysite.jira-dev.com";

    private static final String ABSOLUTE_REQUEST_URI_STRING = "https://mysite.atlassian.net/rest/api/3/issues/issue-1";
    private static final URI ABSOLUTE_REQUEST_URI = URI.create(ABSOLUTE_REQUEST_URI_STRING);

    private static final String RELATIVE_REQUEST_URI_STRING = "/rest/api/3/issues/issue-1";
    private static final URI RELATIVE_REQUEST_URI = URI.create(RELATIVE_REQUEST_URI_STRING);

    private static final String ABSOLUTE_REQUEST_URI_API_GATEWAY_STRING = "https://api.atlassian.com/ex/jira/acloudid/rest/api/3/issues/issue-1";
    private static final URI ABSOLUTE_REQUEST_URI_API_GATEWAY = URI.create(ABSOLUTE_REQUEST_URI_API_GATEWAY_STRING);

    private static final String ABSOLUTE_REQUEST_URI_API_GATEWAY_STG_STRING = "https://api.stg.atlassian.com/ex/jira/acloudid/rest/api/3/issues/issue-1";
    private static final URI ABSOLUTE_REQUEST_URI_API_GATEWAY_STG = URI.create(ABSOLUTE_REQUEST_URI_API_GATEWAY_STG_STRING);

    private final AtlassianConnectProperties atlassianConnectProperties = new AtlassianConnectProperties();

    @Mock
    private AtlassianHostRepository hostRepository;

    private final AtlassianRequestUtils requestUtils = new AtlassianRequestUtils(atlassianConnectProperties);

    private final AtlassianHostUriResolver sut = new AtlassianHostUriResolver(hostRepository, requestUtils);

    public static Stream<Object[]> baseUrlFixtures() {
        return Arrays.stream(new Object[][]{
                {"http://example.com/", "http://example.com/", true},
                {"http://example.com/", "http://example.com/api", true},
                {"http://example.com/", "http://example.com/?some=value", true},
                {"http://example.com/", "https://example.com/", false},
                {"http://example.com/", "/api", true},
                {"http://other-host.com/", "https://example.com/", false}
        });
    }

    @ParameterizedTest
    @MethodSource("baseUrlFixtures")
    void shouldAcceptBaseUrlAsRequestToAuthenticatedHost(String baseUrl, String requestUrl, Boolean expected) {
        assertThat(AtlassianHostUriResolver.isRequestToHost(
                URI.create(requestUrl), createHost(baseUrl)), is(expected));
    }

    @Test
    void shouldResolveToAbsoluteUriWithBase() {
        URI result = sut.resolveToAbsoluteUriWithBase(RELATIVE_REQUEST_URI, BASE_URI_STRING);
        assertThat(result, equalTo(ABSOLUTE_REQUEST_URI));
    }

    @Test
    void shouldResolveToAbsoluteUriWithApiGatewayProd() {
        AtlassianHost host = createTestHost("prod");
        URI result = sut.resolveToAbsoluteUriWithApiGateway(RELATIVE_REQUEST_URI, host);
        assertThat(result, equalTo(ABSOLUTE_REQUEST_URI_API_GATEWAY));
    }

    @Test
    void shouldResolveToAbsoluteUriWithApiGatewayStg() {
        AtlassianHost host = createTestHost("stg");
        URI result = sut.resolveToAbsoluteUriWithApiGateway(RELATIVE_REQUEST_URI, host);
        assertThat(result, equalTo(ABSOLUTE_REQUEST_URI_API_GATEWAY_STG));
    }

    private AtlassianHost createTestHost(String env) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(env.equals("stg") ? BASE_URI_STG_STRING : BASE_URI_STRING);
        host.setProductType(PRODUCT_TYPE);
        host.setCloudId(CLOUD_ID);
        return host;
    }

    private AtlassianHost createHost(String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(baseUrl);
        return host;
    }
}
