package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.ForgeApp;
import com.atlassian.connect.spring.ForgeInvocationToken;
import com.atlassian.connect.spring.ForgeSystemAccessToken;
import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ForgeInvocationTokenAuthenticationFilterTest {

    private ForgeInvocationTokenAuthenticationFilter filter;
    private HttpServletResponse response;
    private FilterChain filterChain;
    private HttpServletRequest request;
    private ForgeSystemAccessTokenRepository mockForgeSystemAccessTokenRepository;
    private ForgeInvocationTokenValidator mockForgeInvocationTokenValidator;
    private LifecycleURLHelper mockLifecycleUrlHelper;
    // https://developer.atlassian.com/platform/forge/remote/essentials/#the-forge-invocation-token--fit-
    private static final String ACCESS_TOKEN = """
        eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcmk6Y2xvdWQ6ZWNvc3lzdGVtOjphcHAvOGRiMzM4MDktMWYzMi00OGJiLThjNTItNTg3N2RhYjQ4MTA3IiwiaXNzIjoiZm9yZ2UvaW52b2NhdGlvbi10b2tlbiIsImlhdCI6MTcwMDE3NTE0OSwibmJmIjoxNzAwMTc1MTQ5LCJleHAiOjE3MDAxNzUxNzQsImp0aSI6ImQ4YTQ5NjI1M2VjOGMxOGE1NDYzMWU0YzgyY2JlZGQ1ZDBhZTg1NzAifQ.Pr6AalO7yfaKTOuvgnxxbudLyN2WnVgim5Kxh3H5Kbk
    """.trim();

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        mockForgeSystemAccessTokenRepository = mock(ForgeSystemAccessTokenRepository.class);
        mockForgeInvocationTokenValidator = mock(ForgeInvocationTokenValidator.class);
        mockLifecycleUrlHelper = mock(LifecycleURLHelper.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
    }

    @Test
    void shouldIgnoreLifecycleEndpoint() throws ServletException, IOException {
        when(mockLifecycleUrlHelper.isRequestToLifecycleURL(eq(request))).thenReturn(false);
        filter = new ForgeInvocationTokenAuthenticationFilter(
            mockForgeSystemAccessTokenRepository,
            mockForgeInvocationTokenValidator,
            mockLifecycleUrlHelper);

        filter.doFilterInternal(request, response, filterChain);

        verify(mockForgeInvocationTokenValidator, times(0)).validate(any(), any());
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void shouldContinueGracefullyIfInvalidForgeInvocationToken() throws ServletException, IOException {
        when(mockLifecycleUrlHelper.isRequestToLifecycleURL(eq(request))).thenReturn(true);
        filter = new ForgeInvocationTokenAuthenticationFilter(
            mockForgeSystemAccessTokenRepository,
            mockForgeInvocationTokenValidator,
            mockLifecycleUrlHelper);
        filter.doFilterInternal(request, response, filterChain);

        verify(mockForgeInvocationTokenValidator, times(0)).validate(any(), any());
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void shouldSaveSystemAccessToken() throws ServletException, IOException {
        ForgeInvocationToken fit = initForgeInvocationToken();
        ForgeSystemAccessToken forgeSystemAccessToken = mock(ForgeSystemAccessToken.class);
        when(mockLifecycleUrlHelper.isRequestToLifecycleURL(request)).thenReturn(false);
        when(mockForgeSystemAccessTokenRepository.save(any(ForgeSystemAccessToken.class))).thenReturn(forgeSystemAccessToken);
        when(mockForgeInvocationTokenValidator.validateForgeInvocationToken(anyString())).thenReturn(fit);
        when(request.getHeader("Authorization")).thenReturn("Bearer example-token");
        when(request.getHeader("x-forge-oauth-system")).thenReturn(ACCESS_TOKEN);
        filter = new ForgeInvocationTokenAuthenticationFilter(
            mockForgeSystemAccessTokenRepository,
            mockForgeInvocationTokenValidator,
            mockLifecycleUrlHelper);
        filter.doFilterInternal(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(request, response);
    }

    /**
     * <a href="https://developer.atlassian.com/platform/forge/remote/essentials/">Forge remote essentials</a>
     */
    ForgeInvocationToken initForgeInvocationToken() {
        ForgeInvocationToken fit = new ForgeInvocationToken();
        ForgeApp app = new ForgeApp();
        app.setId("ari:cloud:ecosystem::app/8db33809-1f32-48bb-8c52-5877dab48107");
        app.setVersion("16");
        app.setApiBaseUrl("https://api.stg.atlassian.com/ex/confluence/d0d52620-3203-4cfa-8db5-f2587155f0dd");
        app.setInstallationId("ari:cloud:ecosystem::installation/0a3a7799-53ae-4a5b-9e7e-03338980abb5");
        fit.setApp(app);
        return fit;
    }
}
