package com.atlassian.connect.spring.internal.auth.asymmetric;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;
import java.util.Objects;

public class AsymmetricAuthenticationToken extends AbstractAuthenticationToken {

    private AsymmetricCredentials credentials;

    private final String tenantBaseUrl;

    public AsymmetricAuthenticationToken(AsymmetricCredentials credentials, String tenantBaseUrl) {
        super(Collections.emptySet());
        this.credentials = credentials;
        this.tenantBaseUrl = tenantBaseUrl;
    }

    public String getTenantBaseUrl() {
        return tenantBaseUrl;
    }

    @Override
    public AsymmetricCredentials getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AsymmetricAuthenticationToken that = (AsymmetricAuthenticationToken) o;
        return Objects.equals(credentials, that.credentials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), credentials);
    }
}
