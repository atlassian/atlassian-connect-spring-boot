package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AbstractConnectAuthenticationProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.InvalidKeyException;
import com.atlassian.connect.spring.internal.jwt.JwtExpiredException;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidSigningAlgorithmException;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtParser;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.jwt.SymmetricJwtReader;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Map;
import java.util.Optional;

/**
 * An {@link AuthenticationProvider} for JSON Web Tokens, issued by an Atlassian host or by the add-on itself.
 * <p>
 * For JWTs issued by Atlassian hosts, in addition to verifying the signature of the JWT, the query string hash claim
 * specific to Atlassian Connect is also verified.
 */
public class JwtAuthenticationProvider extends AbstractConnectAuthenticationProvider {

    private static final Class<JwtAuthenticationToken> TOKEN_CLASS = JwtAuthenticationToken.class;

    public JwtAuthenticationProvider(AddonDescriptorLoader addonDescriptorLoader, AtlassianHostRepository hostRepository) {
        super(addonDescriptorLoader, hostRepository);
    }

    @Override
    public boolean supports(Class<?> authenticationClass) {
        return authenticationClass.equals(TOKEN_CLASS);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        JwtCredentials jwtCredentials = getJwtCredentials(authentication);
        JWTClaimsSet unverifiedClaims = parseToken(jwtCredentials.getRawJwt());
        log.debug("Parsed JWT: {}", unverifiedClaims);
        String hostClientKey = getHostClientKeyFromSelfAuthenticationToken(unverifiedClaims)
                .orElseGet(unverifiedClaims::getIssuer);
        AtlassianHost host = getHost(hostClientKey);

        try {
            JWTClaimsSet verifiedClaims = verifyToken(jwtCredentials, host);
            String queryStringHash = computeQueryStringHash(jwtCredentials);
            AtlassianHostUser hostUser = createHostUserFromContextClaim(host, verifiedClaims)
                    .orElseGet(() -> createHostUserFromSubjectClaim(host, verifiedClaims));
            return new JwtAuthentication(hostUser, verifiedClaims, queryStringHash);
        } catch (ClassCastException e) {
            log.error("Context claim present, but not a JSON object. Unable to parse");
            throw new InvalidJwtException("Unable to parse context in JWT", e);
        } catch (JwtInvalidSigningAlgorithmException e) {
            return null;
        }
    }

    private Optional<AtlassianHostUser> createHostUserFromContextClaim(AtlassianHost host, JWTClaimsSet verifiedClaims) throws ClassCastException {
        Object context = verifiedClaims.getClaim("context");
        if (context != null) {
            @SuppressWarnings("unchecked")
            Map<String, Object> user = (Map<String, Object>) ((Map<String, Object>) context).get("user");
            if (user != null) {
                JSONObject userJsonObject = new JSONObject(user);
                String accountId = userJsonObject.getAsString("accountId");

                return Optional.of(
                        AtlassianHostUser.builder(host)
                                .withUserAccountId(accountId)
                                .build());
            }
        }

        return Optional.empty();
    }

    private JwtCredentials getJwtCredentials(Authentication authentication) {
        JwtAuthenticationToken authenticationToken = TOKEN_CLASS.cast(authentication);
        return authenticationToken.getCredentials();
    }

    private JWTClaimsSet parseToken(String jwt) throws AuthenticationException {
        try {
            return new JwtParser().parse(jwt);
        } catch (JwtParseException e) {
            log.error(e.getMessage());
            throw new InvalidJwtException(e.getMessage(), e);
        }
    }

    private JWTClaimsSet verifyToken(JwtCredentials jwtCredentials, AtlassianHost host) throws AuthenticationException, JwtInvalidSigningAlgorithmException {
        JWTClaimsSet claims;
        try {
            String sharedSecret = host.getSharedSecret();
            claims = new SymmetricJwtReader(new MACVerifier(sharedSecret)).readAndVerify(jwtCredentials.getRawJwt(), null);
        } catch (JwtParseException e) {
            log.error(e.getMessage());
            throw new InvalidJwtException(e.getMessage(), e);
        } catch (JwtExpiredException e) {
            log.error(e.getMessage());
            throw new CredentialsExpiredException(e.getMessage());
        } catch (JwtInvalidSigningAlgorithmException e) {
            throw e;
        } catch (JwtVerificationException e) {
            log.error(e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        } catch (JOSEException e) {
            log.error(e.getMessage());
            throw new InvalidKeyException(e);
        }

        log.debug("Verified JWT for host {} ({}) ", host.getBaseUrl(), host.getClientKey());

        return claims;
    }
}
