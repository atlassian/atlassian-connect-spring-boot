package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AbstractAuthentication;
import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

/**
 * An authentication object representing a verified and accepted JSON Web Token.
 *
 * @see JwtAuthenticationToken
 */
public class JwtAuthentication extends AbstractAuthentication {

    public static final String ROLE_JWT = "ROLE_JWT";

    public JwtAuthentication(AtlassianHostUser hostUser, JWTClaimsSet claims, String queryStringHash) {
        super(hostUser, claims, queryStringHash);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(ROLE_JWT));
    }

    @Override
    public String getName() {
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(hostUser.getHost().getClientKey());
        hostUser.getUserAccountId().ifPresent(userAccountId -> nameBuilder.append(String.format(" (%s)", userAccountId)));

        return nameBuilder.toString();
    }
}
