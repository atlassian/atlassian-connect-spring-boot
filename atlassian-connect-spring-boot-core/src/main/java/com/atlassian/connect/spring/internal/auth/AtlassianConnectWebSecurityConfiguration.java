package com.atlassian.connect.spring.internal.auth;

import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;

import java.util.concurrent.TimeUnit;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class AtlassianConnectWebSecurityConfiguration {

    private static final long ONE_YEAR_IN_SECONDS = TimeUnit.SECONDS.convert(365, TimeUnit.DAYS);

    private final WebEndpointProperties webEndpointProperties;

    public AtlassianConnectWebSecurityConfiguration(WebEndpointProperties webEndpointProperties) {
        this.webEndpointProperties = webEndpointProperties;
    }

    @Bean
    @Order(SecurityProperties.BASIC_AUTH_ORDER + 1)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.headers(headers -> headers.frameOptions(frameOptions ->
                frameOptions.disable()
                        .httpStrictTransportSecurity(policy -> policy.includeSubDomains(false).maxAgeInSeconds(ONE_YEAR_IN_SECONDS))
                        .referrerPolicy(referrerPolicy -> referrerPolicy.policy(ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN))));
        http.csrf(AbstractHttpConfigurer::disable);
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(authz -> {
                    String managementPath = webEndpointProperties.getBasePath();
                    if (managementPath != null) {
                        authz.requestMatchers(managementPath + "/**").authenticated();
                    }
                    authz.anyRequest().permitAll();
                }
        );
        http.httpBasic(withDefaults());
        return http.build();
    }

    @Bean
    @Order(SecurityProperties.BASIC_AUTH_ORDER + 1)
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }
}
