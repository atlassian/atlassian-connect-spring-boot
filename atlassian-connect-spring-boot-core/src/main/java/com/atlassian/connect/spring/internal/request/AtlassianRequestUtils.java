package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.atlassian.connect.spring.internal.AtlassianConnectProperties.RequestUriEncoding.DEFAULT;
import static com.atlassian.connect.spring.internal.AtlassianConnectProperties.RequestUriEncoding.NONE;


/**
 * Common request utilities.
 */
@Component
public class AtlassianRequestUtils {

    public static final String PRODUCTION_API_GATEWAY_HOST_URL = "api.atlassian.com";
    public static final String STAGING_API_GATEWAY_URL_HOST_URL = "api.stg.atlassian.com";
    public static final String FEDRAMP_SANDBOX_API_GATEWAY_URL_HOST_URL = "api.atlassian-fex.com";
    private static final String PRODUCTION_API_GATEWAY_URL_PREFIX = "https://" + PRODUCTION_API_GATEWAY_HOST_URL + "/ex";
    private static final String STAGING_API_GATEWAY_URL_PREFIX = "https://" + STAGING_API_GATEWAY_URL_HOST_URL + "/ex";
    private static final String FEDRAMP_SANDBOX_API_GATEWAY_URL_PREFIX = "https://" + FEDRAMP_SANDBOX_API_GATEWAY_URL_HOST_URL + "/ex";
    private static final String STAGING_HOST_POSTFIX = ".jira-dev.com";
    private static final String FEDRAMP_SANDBOX_HOST_POSTFIX = ".atlassian-fex.net";
    private static final String PRODUCT_JIRA = "jira";
    private static final String PRODUCT_CONFLUENCE = "confluence";
    private static final URI PRODUCTION_AUTHORIZATION_SERVER_TOKEN_URL = URI.create("https://auth.atlassian.com/oauth/token");
    private static final URI STAGING_AUTHORIZATION_SERVER_TOKEN_URL = URI.create("https://auth.stg.atlassian.com/oauth/token");
    private static final URI FEDRAMP_SANDBOX_AUTHORIZATION_SERVER_TOKEN_URL = URI.create("https://auth.atlassian-fex.com/oauth/token");
    private static final String PRODUCTION_IDENTITY_AUDIENCE = "api.atlassian.com";
    private static final String STAGING_IDENTITY_AUDIENCE = "api.stg.atlassian.com";
    private static final String FEDRAMP_SANDBOX_IDENTITY_AUDIENCE = "api.atlassian-fex.com";

    private final AtlassianConnectProperties atlassianConnectProperties;

    public AtlassianRequestUtils(AtlassianConnectProperties atlassianConnectProperties) {
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    public boolean isStagingHost(AtlassianHost host) {
        return URI.create(host.getBaseUrl()).getHost().endsWith(STAGING_HOST_POSTFIX);
    }

    public boolean isFedRAMPSandboxHost(AtlassianHost host) {
        return URI.create(host.getBaseUrl()).getHost().endsWith(FEDRAMP_SANDBOX_HOST_POSTFIX);
    }

    public URI getAuthServerTokenUrl(AtlassianHost host) {
        if (isFedRAMPSandboxHost(host)) {
            return FEDRAMP_SANDBOX_AUTHORIZATION_SERVER_TOKEN_URL;
        }
        return isStagingHost(host) ? STAGING_AUTHORIZATION_SERVER_TOKEN_URL : PRODUCTION_AUTHORIZATION_SERVER_TOKEN_URL;
    }

    public String getAudience(AtlassianHost host) {
        if (isFedRAMPSandboxHost(host)) {
            return FEDRAMP_SANDBOX_IDENTITY_AUDIENCE;
        }
        return isStagingHost(host) ? STAGING_IDENTITY_AUDIENCE : PRODUCTION_IDENTITY_AUDIENCE;
    }

    public String getApiGatewayUrlPrefix(AtlassianHost host) {
        if (isFedRAMPSandboxHost(host)) {
            return FEDRAMP_SANDBOX_API_GATEWAY_URL_PREFIX;
        }
        return isStagingHost(host) ? STAGING_API_GATEWAY_URL_PREFIX : PRODUCTION_API_GATEWAY_URL_PREFIX;
    }

    public String getProductionApiGatewayHostUrl() {
        return PRODUCTION_API_GATEWAY_HOST_URL;
    }

    public boolean isValidProductType(String productType) { // only support Jira and Confluence, right? but maybe we don't need to valid the incoming productType?
        return productType.equals(PRODUCT_JIRA) || productType.equals(PRODUCT_CONFLUENCE);
    }

    public static void validateAuthorizationGrantType(AuthorizationGrantType grantType, AuthorizationGrantType desiredGrantType) {
        if (!(grantType.equals(desiredGrantType))) {
            throw new IllegalStateException("Authorization grant type " + grantType.getValue() + " does not match the desired type " + desiredGrantType.getValue() + ", can not authorize.");
        }
    }

    public URI getRelativeUriToResolve(URI uri, URI baseUri) {
        String basePath = Optional.ofNullable(baseUri.getPath()).orElse("");
        // If UriEncoding property is "none" we don't want to decode the path
        String path = atlassianConnectProperties.getUriEncoding() == NONE ? uri.getRawPath() : uri.getPath();
        // Ensure we don't double up slashes between base path and uri path
        if (basePath.endsWith("/") && path != null && path.startsWith("/")) {
            path = path.substring(1);
        }
        String pathToResolve = basePath + Optional.ofNullable(path).orElse("");

        try {
            if (atlassianConnectProperties.getUriEncoding() == DEFAULT) {
                return new URI(null, null, pathToResolve, uri.getQuery(), uri.getFragment());
            } else { // "none"
                String query = uri.getRawQuery();
                String fragment = uri.getRawFragment();
                return UriComponentsBuilder.newInstance()
                        .path(pathToResolve)
                        .query(query)
                        .fragment(fragment)
                        .build(true)
                        .toUri();
            } // values other than "default" or "none" will be handled by Configuration properties and throw IllegalArgumentException
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }
}
