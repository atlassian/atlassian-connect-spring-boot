package com.atlassian.connect.spring.internal.request.frc;

public enum ForgeRemoteRequestAuthType {
    APP,
    USER,
    NONE
}
