package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.ForgeRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
public class ForgeRemoteEnforcer implements BeanPostProcessor {

    private final Environment env;

    private static final Logger log = LoggerFactory.getLogger(ForgeRemoteEnforcer.class);

    public ForgeRemoteEnforcer(Environment env) {
        this.env = env;
    }

    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        Method[] methods = bean.getClass().getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(ForgeRemote.class)) {
                log.info("Method %s of %s is annotated with @ForgeRemote".formatted(method.getName(), bean.getClass()));
                String appId = env.getProperty("app.id");
                if (appId == null || appId.isEmpty()) {
                    throw new IllegalStateException("app.id environment variable is not set");
                }
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        return bean;
    }
}
