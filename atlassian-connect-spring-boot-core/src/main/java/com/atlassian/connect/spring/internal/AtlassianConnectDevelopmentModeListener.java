package com.atlassian.connect.spring.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AtlassianConnectDevelopmentModeListener {

    private static final Logger log = LoggerFactory.getLogger(AtlassianConnectDevelopmentModeListener.class);

    private final AtlassianConnectProperties atlassianConnectProperties;

    public AtlassianConnectDevelopmentModeListener(AtlassianConnectProperties atlassianConnectProperties) {
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    @EventListener
    public void contextRefreshed(ContextRefreshedEvent event) {
        if (atlassianConnectProperties.isAllowReinstallMissingHost()) {
            log.warn("Accepting installations signed by unknown hosts. This setting poses a security risk, and should not be used in production deployments.");
        }

        if (atlassianConnectProperties.isDebugAllJs()) {
            log.warn("Enabled debug mode for the JavaScript API. This mode leads to reduced performance, and should not be used in production deployments.");
        }
    }
}
