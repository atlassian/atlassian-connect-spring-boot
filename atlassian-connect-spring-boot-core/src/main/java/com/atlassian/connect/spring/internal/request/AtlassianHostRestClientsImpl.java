package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.jwt.JwtGenerator;
import com.atlassian.connect.spring.internal.request.jwt.JwtSigningRestTemplateFactory;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2JwtTokenService;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2RestTemplateFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class AtlassianHostRestClientsImpl implements AtlassianHostRestClients {

    private final JwtSigningRestTemplateFactory jwtSigningRestTemplateFactory;

    private final JwtGenerator jwtGenerator;

    private final OAuth2RestTemplateFactory oauth2RestTemplateFactory;

    private final OAuth2JwtTokenService oAuth2JwtTokenService;

    private final AtlassianConnectSecurityContextHelper securityContextHelper;

    @Autowired
    public AtlassianHostRestClientsImpl(JwtSigningRestTemplateFactory jwtSigningRestTemplateFactory,
                                        JwtGenerator jwtGenerator,
                                        OAuth2RestTemplateFactory oauth2RestTemplateFactory,
                                        OAuth2JwtTokenService oAuth2JwtTokenService,
                                        AtlassianConnectSecurityContextHelper securityContextHelper) {
        this.jwtSigningRestTemplateFactory = jwtSigningRestTemplateFactory;
        this.jwtGenerator = jwtGenerator;
        this.oauth2RestTemplateFactory = oauth2RestTemplateFactory;
        this.oAuth2JwtTokenService = oAuth2JwtTokenService;
        this.securityContextHelper = securityContextHelper;
    }

    @Override
    public RestTemplate authenticatedAsAddon() {
        return authenticatedAsAddon(AddonAuthenticationType.JWT);
    }

    @Override
    public RestTemplate authenticatedAsAddon(AtlassianHost host) {
        return authenticatedAsAddon(host, AddonAuthenticationType.JWT);
    }

    @Override
    public RestTemplate authenticatedAsAddon(AddonAuthenticationType auth) {
        return switch (auth) {
            case JWT -> jwtSigningRestTemplateFactory.getJwtRestTemplate();
        };
    }

    @Override
    public RestTemplate authenticatedAsAddon(AtlassianHost host, AddonAuthenticationType auth) {
        return switch (auth) {
            case JWT -> jwtSigningRestTemplateFactory.getJwtRestTemplate(host);
        };
    }

    @Override
    public String createJwt(HttpMethod method, URI uri) {
        return jwtGenerator.createJwtToken(method, uri);
    }

    @Override
    public RestTemplate authenticatedAsHostActor() {
        AtlassianHostUser hostUser = securityContextHelper.getHostUserFromSecurityContext()
                .orElseThrow(() -> new IllegalStateException("Was asked to authenticate the rest client as " +
                        "the current acting user on the host, " +
                        "but none could be inferred from the current request"));
        return authenticatedAs(hostUser);
    }

    @Override
    public RestTemplate authenticatedAs(AtlassianHostUser hostUser) {
        return oauth2RestTemplateFactory.getOAuth2RestTemplate(hostUser);
    }

    @Override
    public OAuth2AccessToken getAccessToken(AtlassianHostUser hostUser) {
        return oAuth2JwtTokenService.getAccessToken(hostUser);
    }
}
