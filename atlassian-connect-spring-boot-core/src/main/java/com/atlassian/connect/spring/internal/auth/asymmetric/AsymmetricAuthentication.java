package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AbstractAuthentication;
import com.nimbusds.jwt.JWTClaimsSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class AsymmetricAuthentication extends AbstractAuthentication {

    public static final String ROLE_ASYMMETRIC_JWT = "ROLE_ASYMMETRIC_JWT";

    public AsymmetricAuthentication(AtlassianHostUser hostUser, JWTClaimsSet claims, String queryStringHash) {
        super(hostUser, claims, queryStringHash);
    }

    @Override
    public String getName() {
        StringBuilder nameBuilder = new StringBuilder();
        if (hostUser != null) {
            nameBuilder.append(hostUser.getHost().getClientKey());
            hostUser.getUserAccountId().ifPresent(userAccountId -> nameBuilder.append(String.format(" (%s)", userAccountId)));
        }

        return nameBuilder.toString();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(ROLE_ASYMMETRIC_JWT));
    }
}
