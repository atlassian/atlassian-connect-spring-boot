package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.ForgeInvocationToken;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimNames;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;

@Component
public class ForgeInvocationTokenValidator {
    private final AtlassianConnectProperties atlassianConnectProperties;

    private final Environment environment;

    @Autowired
    public ForgeInvocationTokenValidator(AtlassianConnectProperties atlassianConnectProperties, Environment env) {
        this.environment = env;
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    private static final Logger log = LoggerFactory.getLogger(ForgeInvocationTokenValidator.class);

    public String getBearerToken(String authorizationHeader) {
        if (authorizationHeader == null || !authorizationHeader.toLowerCase().startsWith(OAuth2AccessToken.TokenType.BEARER.getValue().toLowerCase() + " ")) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(401), "Invalid Authorization header");
        }

        return authorizationHeader.split(" ")[1];
    }

    public JWTClaimsSet validate(final String authorizationHeader, final String appId) {
        try {
            final String invocationToken = getBearerToken(authorizationHeader);

            JWKSet publicKeys = JWKSet.load(new URL(atlassianConnectProperties.getForgeInvocationTokenJwksUrl()));
            JWKSource<SecurityContext> keySource = new ImmutableJWKSet<>(publicKeys);

            JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<>(
                    JWSAlgorithm.RS256, keySource);
            ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();
            jwtProcessor.setJWSKeySelector(keySelector);

            jwtProcessor.setJWTClaimsSetVerifier(new DefaultJWTClaimsVerifier<>(
                    new JWTClaimsSet.Builder().issuer("forge/invocation-token").audience(appId).build(),
                    new HashSet<>(Arrays.asList(
                            JWTClaimNames.ISSUER,
                            JWTClaimNames.AUDIENCE))));
            JWTClaimsSet jwtClaimsSet = jwtProcessor.process(invocationToken, null);
            log.info("JWK validated");
            return jwtClaimsSet;
        } catch (ParseException | JOSEException | BadJOSEException | IOException e) {
            log.error("JWK is not valid {}", e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid Authorization header");
        }
    }

    public ForgeInvocationToken decode(final JWTClaimsSet jwtClaimsSet) {
        try {
            final String jsonClaims = jwtClaimsSet.toString();

            final ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readValue(jsonClaims, ForgeInvocationToken.class);
        } catch (JsonProcessingException e) {
            log.error("Error decoding JWT claims {}", e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid Authorization header");
        } catch (IllegalArgumentException e) {
            log.error("Invalid JWT claims {}", e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid JWT claims");
        }
    }

    public ForgeInvocationToken validateForgeInvocationToken(final String authorizationHeader) {
        JWTClaimsSet validatedClaimSet = validate(authorizationHeader, environment.getProperty("app.id"));

        return decode(validatedClaimSet);
    }

}
