package com.atlassian.connect.spring.internal.lifecycle;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectPerTenantEnvironmentConfiguration;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricAuthenticationProvider;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricAuthenticationToken;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricCredentials;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidAsymmetricJwtException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpServletRequest;
import jakarta.annotation.Nonnull;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

@Component
public class LifecycleAuthenticator {
    private static final String AUTHORIZATION_HEADER_SCHEME_PREFIX = "JWT ";
    private static final Logger log = LoggerFactory.getLogger(LifecycleAuthenticator.class);
    private final AsymmetricAuthenticationProvider asymmetricAuthenticationProvider;

    public LifecycleAuthenticator(
                                          AddonDescriptorLoader addonDescriptorLoader,
                                          AtlassianHostRepository hostRepository,
                                          AsymmetricPublicKeyProvider asymmetricPublicKeyProvider,
                                          AtlassianConnectPerTenantEnvironmentConfiguration perTenantEnvironmentConfiguration,
                                          AtlassianConnectProperties atlassianConnectProperties) {
        this.asymmetricAuthenticationProvider = new AsymmetricAuthenticationProvider(addonDescriptorLoader, hostRepository, perTenantEnvironmentConfiguration::getPublicKeyBaseUrl, perTenantEnvironmentConfiguration::getFallbackPublicKeyBaseUrl, asymmetricPublicKeyProvider, atlassianConnectProperties);
    }

    private static Optional<String> getJwtFromHeader(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!ObjectUtils.isEmpty(authHeader) && authHeader.startsWith(AUTHORIZATION_HEADER_SCHEME_PREFIX)) {
            String jwt = authHeader.substring(AUTHORIZATION_HEADER_SCHEME_PREFIX.length());
            optionalJwt = Optional.of(jwt);
        }

        return optionalJwt;
    }

    public void setAuthentication(@Nonnull HttpServletRequest request, String tenantBaseUrl) throws AuthenticationException {
        Optional<String> optionalJwt = getJwtFromHeader(request);

        if (optionalJwt.isEmpty()) {
            String message = "Failed to authenticate install/uninstall request due to missing JWT token.";
            log.error(message);
            throw new InvalidAsymmetricJwtException(message);
        }

        AsymmetricAuthenticationToken authenticationRequest = createPKIAuthenticationToken(request, optionalJwt.get(), tenantBaseUrl);
        Authentication authenticationResult;

        try {
            authenticationResult = asymmetricAuthenticationProvider.authenticate(authenticationRequest);
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        } catch (AuthenticationException e) {
            String message = "Failed to authenticate install/uninstall lifecycle request.";
            log.error(message, e);
            throw new InvalidAsymmetricJwtException(message);
        }
    }

    private AsymmetricAuthenticationToken createPKIAuthenticationToken(HttpServletRequest request, String jwt, String tenantBaseUrl) {
        log.debug("Retrieved JWT from request");
        CanonicalHttpServletRequest canonicalHttpServletRequest = new CanonicalHttpServletRequest(request);
        AsymmetricCredentials credentials = new AsymmetricCredentials(jwt, canonicalHttpServletRequest);
        return new AsymmetricAuthenticationToken(credentials, tenantBaseUrl);
    }
}
