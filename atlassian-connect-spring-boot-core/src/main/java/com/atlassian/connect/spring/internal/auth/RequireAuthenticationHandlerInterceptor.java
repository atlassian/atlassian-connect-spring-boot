package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;
import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.ForgeRemote;
import com.atlassian.connect.spring.IgnoreJwt;
import com.atlassian.connect.spring.internal.auth.frc.ForgeAuthentication;
import com.atlassian.connect.spring.internal.auth.frc.ForgeConnectMappingService;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidClaimException;
import jakarta.annotation.Nonnull;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import java.lang.annotation.Annotation;
import java.util.Optional;

/**
 * A handler interceptor that:
 *  - enforces JWT authentication for all handler methods not annotated with {@link IgnoreJwt}.
 *  - enforces Forge Remote authentication for all handler methods annotated with {@link ForgeRemote}.
 *  - adds Connect context parameters into `AtlassianHostUser` for all handler methods annotated with {@link ForgeRemote}(associateConnect=true)
 */
@Component
public class RequireAuthenticationHandlerInterceptor implements HandlerInterceptor {

    private static final Logger log = LoggerFactory.getLogger(RequireAuthenticationHandlerInterceptor.class);

    private final AddonDescriptorLoader addonDescriptorLoader;
    private final ForgeConnectMappingService forgeConnectMappingService;

    public RequireAuthenticationHandlerInterceptor(AddonDescriptorLoader addonDescriptorLoader, ForgeConnectMappingService forgeConnectMappingService) {
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.forgeConnectMappingService = forgeConnectMappingService;
    }

    @Override
    public boolean preHandle(@Nonnull HttpServletRequest request,
                             @Nonnull HttpServletResponse response,
                             @Nonnull Object handler) throws Exception {
        if (isApplicableDispatcherType(request)) {
            if (handlerRequiresForgeRemoteAuthentication(handler)) {
                if (!requestHasForgeInvocationToken()) {
                    log.info("Rejected incoming request for controller requiring Forge Remote Context authentication ({} {})", request.getMethod(), request.getRequestURI());
                    response.sendError(HttpStatus.UNAUTHORIZED.value());
                    return false;
                }
                if (handlerRequiresConnectToForgeMapping(handler)) {
                    addConnectMappingToHostUser();
                }
                return true;
            }
            if (handlerRequiresJwtAuthentication(handler)) {
                if (!requestIsSigned()) {
                    log.info("Rejected incoming request for controller requiring JWT authentication ({} {})", request.getMethod(), request.getRequestURI());
                    response.addHeader(HttpHeaders.WWW_AUTHENTICATE, String.format("JWT realm=\"%s\"", addonDescriptorLoader.getDescriptor().getKey()));
                    response.sendError(HttpStatus.UNAUTHORIZED.value());
                    return false;
                }

                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication instanceof JwtAuthentication jwtAuthentication) {
                    try {
                        jwtAuthentication.validateQueryStringHash(handlerRequiresQshValidation(handler));
                    } catch (JwtInvalidClaimException e) {
                        // qsh mismatch
                        response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void addConnectMappingToHostUser() {
        ForgeAuthentication forgeAuthentication = (ForgeAuthentication) SecurityContextHolder.getContext().getAuthentication();
        Optional<AtlassianHostUser> hostUser = forgeConnectMappingService.getAtlassianHostUserFromForgeInvocationToken(forgeAuthentication.getDetails().getForgeInvocationToken());
        Authentication authenticationResult = new ForgeAuthentication(forgeAuthentication.getDetails(), hostUser.orElse(null));

        SecurityContextHolder.getContext().setAuthentication(authenticationResult);
    }


    private boolean handlerRequiresJwtAuthentication(Object handler) {
        return handler instanceof HandlerMethod handlerMethod && !handlerHasAnnotation(handlerMethod, IgnoreJwt.class);
    }

    private boolean handlerRequiresQshValidation(Object handler) {
        return handler instanceof HandlerMethod handlerMethod && !handlerHasAnnotation(handlerMethod, ContextJwt.class);
    }

    private boolean handlerRequiresForgeRemoteAuthentication(Object handler) {
        return handler instanceof HandlerMethod handlerMethod && handlerHasAnnotation(handlerMethod, ForgeRemote.class);
    }

    private boolean handlerRequiresConnectToForgeMapping(Object handler) {
        return handler instanceof HandlerMethod handlerMethod && forgeRemoteAnnotationRequiresConnectAssociation(handlerMethod);
    }

    private boolean forgeRemoteAnnotationRequiresConnectAssociation(HandlerMethod method) {
        return method.getMethod().isAnnotationPresent(ForgeRemote.class) &&
                method.getMethod().getAnnotation(ForgeRemote.class).associateConnect();
    }

    private <T extends Annotation> boolean handlerHasAnnotation(HandlerMethod method, Class<T> annotationClass) {
        return method.getMethod().isAnnotationPresent(annotationClass)
                || method.getBeanType().isAnnotationPresent(annotationClass);
    }

    private boolean requestIsSigned() {
        boolean signed = false;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            signed = authentication.isAuthenticated() && (authentication.getPrincipal() instanceof AtlassianHostUser);
        }
        return signed;
    }

    private boolean requestHasForgeInvocationToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        return authentication.isAuthenticated() && (authentication.getDetails() instanceof ForgeApiContext);
    }

    private boolean isApplicableDispatcherType(HttpServletRequest request) {
        DispatcherType dispatcherType = request.getDispatcherType();
        return !dispatcherType.equals(DispatcherType.ASYNC) && !dispatcherType.equals(DispatcherType.ERROR);
    }
}
