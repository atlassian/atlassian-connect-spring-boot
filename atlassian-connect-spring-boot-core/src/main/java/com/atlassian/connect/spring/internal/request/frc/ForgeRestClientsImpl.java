package com.atlassian.connect.spring.internal.request.frc;

import com.atlassian.connect.spring.AtlassianForgeRestClients;
import com.atlassian.connect.spring.ForgeRequestProductMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ForgeRestClientsImpl extends AtlassianForgeRestClients {

    private final ForgeRestTemplateFactory forgeRestTemplateFactory;

    @Autowired
    public ForgeRestClientsImpl(ForgeRestTemplateFactory forgeRestTemplateFactory) {
        this.forgeRestTemplateFactory = forgeRestTemplateFactory;
    }

    @Override
    public ForgeRequestProductMethods asApp() {
        return new ForgeRequestProductMethodsImpl(ForgeRemoteRequestAuthType.APP, forgeRestTemplateFactory);
    }

    @Override
    public ForgeRequestProductMethods asApp(String installationId) {
        forgeRestTemplateFactory.setInstallationId(installationId);
        return new ForgeRequestProductMethodsImpl(ForgeRemoteRequestAuthType.APP, forgeRestTemplateFactory);
    }

    @Override
    public ForgeRequestProductMethods asUser() {
        return new ForgeRequestProductMethodsImpl(ForgeRemoteRequestAuthType.USER, forgeRestTemplateFactory);
    }

    @Override
    public RestTemplate request() {
        return forgeRestTemplateFactory.getForgeRestTemplate(ForgeRemoteRequestAuthType.NONE, ForgeRemoteRequestType.OTHER);
    }

    @Override
    public RestTemplate requestConfluence() {
        return forgeRestTemplateFactory.getForgeRestTemplate(ForgeRemoteRequestAuthType.NONE, ForgeRemoteRequestType.CONFLUENCE);
    }

    @Override
    public RestTemplate requestJira() {
        return forgeRestTemplateFactory.getForgeRestTemplate(ForgeRemoteRequestAuthType.NONE, ForgeRemoteRequestType.JIRA);
    }
}
