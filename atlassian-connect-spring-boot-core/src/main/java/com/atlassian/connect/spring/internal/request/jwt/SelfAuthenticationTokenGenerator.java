package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * A generator of JSON Web Tokens for authenticating requests from the add-on to itself.
 */
@Component
public class SelfAuthenticationTokenGenerator {

    /**
     * The name of the JWT claim used for the client key of the Atlassian host in self-authentication tokens
     */
    public static final String HOST_CLIENT_KEY_CLAIM = "clientKey";

    private final AddonDescriptorLoader addonDescriptorLoader;

    private final AtlassianConnectProperties atlassianConnectProperties;

    public SelfAuthenticationTokenGenerator(AddonDescriptorLoader addonDescriptorLoader,
                                            AtlassianConnectProperties atlassianConnectProperties) {
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    public String createSelfAuthenticationToken(AtlassianHostUser hostUser) {
        Duration expirationTime = Duration.of(atlassianConnectProperties.getSelfAuthenticationExpirationTime(), ChronoUnit.SECONDS);
        JwtBuilder jwtBuilder = new JwtBuilder(expirationTime)
                .issuer(addonDescriptorLoader.getDescriptor().getKey())
                .audience(addonDescriptorLoader.getDescriptor().getKey())
                .claim(HOST_CLIENT_KEY_CLAIM, hostUser.getHost().getClientKey())
                .claim("qsh", "context-qsh")
                .signature(hostUser.getHost().getSharedSecret());

        hostUser.getUserAccountId().ifPresent(jwtBuilder::subject);

        return jwtBuilder.build();
    }
}
