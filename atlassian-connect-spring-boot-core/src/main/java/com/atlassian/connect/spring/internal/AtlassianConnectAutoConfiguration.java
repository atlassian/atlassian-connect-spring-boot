package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import com.atlassian.connect.spring.internal.auth.frc.ForgeInvocationTokenAuthenticationFilter;
import com.atlassian.connect.spring.internal.auth.frc.ForgeInvocationTokenValidator;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthenticationFilter;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2JwtClientRegistrationRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for Atlassian Connect add-ons.
 */
@Configuration
@ComponentScan(basePackageClasses = {AtlassianConnectAutoConfiguration.class})
@EnableConfigurationProperties(AtlassianConnectProperties.class)
@ConditionalOnResource(resources = AddonDescriptorLoader.DESCRIPTOR_RESOURCE_PATH)
@EnableCaching
@EnableAsync
public class AtlassianConnectAutoConfiguration {

    private final ForgeInvocationTokenValidator validator;

    public AtlassianConnectAutoConfiguration(ForgeInvocationTokenValidator validator) {
        this.validator = validator;
    }

    @Bean
    public FilterRegistrationBean<JwtAuthenticationFilter> jwtAuthenticationFilterRegistrationBean(AuthenticationManager authenticationManager,
                                                                                                   AtlassianConnectProperties atlassianConnectProperties,
                                                                                                   ServerProperties serverProperties,
                                                                                                   LifecycleURLHelper lifecycleURLHelper) {
        FilterRegistrationBean<JwtAuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new JwtAuthenticationFilter(authenticationManager,
                atlassianConnectProperties, serverProperties, lifecycleURLHelper));
        registrationBean.setOrder(atlassianConnectProperties.getJwtFilterOrder());
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<ForgeInvocationTokenAuthenticationFilter> forgeInvocationTokenAuthenticationFilterRegistrationBean(
        AtlassianConnectProperties atlassianConnectProperties,
        ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository,
        LifecycleURLHelper lifecycleURLHelper) {
        FilterRegistrationBean<ForgeInvocationTokenAuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ForgeInvocationTokenAuthenticationFilter(
            forgeSystemAccessTokenRepository,
            validator,
            lifecycleURLHelper));
        registrationBean.setOrder(atlassianConnectProperties.getForgeFilterOrder());
        return registrationBean;
    }

    @Bean
    OAuth2AuthorizedClientService authorizedClientService(@Qualifier("oauth2-jwt-impersonation") OAuth2JwtClientRegistrationRepository clientRegistrationRepository) {
        return new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository);
    }


    @Configuration
    @PropertySource("classpath:config/default.properties")
    static class Defaults {
    }

    @Configuration
    @Profile("production")
    @PropertySource({"classpath:config/default.properties", "classpath:config/production.properties"})
    static class Production {
    }
}
