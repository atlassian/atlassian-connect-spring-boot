package com.atlassian.connect.spring.internal.request.frc;

import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.internal.auth.frc.ForgeSecurityContextRetriever;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Component
public class ForgeRestTemplateFactory {
    private final RestTemplateBuilder restTemplateBuilder;
    private final AtlassianHostUriResolver hostUriResolver;
    private final ForgeSecurityContextRetriever forgeContextRetriever;
    private final ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository;
    private Optional<String> installationId = Optional.empty();

    @Value("${api.host:api.atlassian.com}")
    private String forgeApiHost;

    public ForgeRestTemplateFactory(RestTemplateBuilder restTemplateBuilder,
                                    AtlassianHostUriResolver hostUriResolver,
                                    ForgeSecurityContextRetriever forgeContextRetriever,
                                    ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.hostUriResolver = hostUriResolver;
        this.forgeContextRetriever = forgeContextRetriever;
        this.forgeSystemAccessTokenRepository = forgeSystemAccessTokenRepository;
    }

    public RestTemplate getForgeRestTemplate(ForgeRemoteRequestAuthType provider, ForgeRemoteRequestType remote) {
        ForgeHttpRequestInterceptor requestInterceptor = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);
        getInstallationId().ifPresent(requestInterceptor::setInstallationId);
        return restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }

    public Optional<String> getInstallationId() {
        return this.installationId;
    }

    public void setInstallationId(String installationId) {
        this.installationId = Optional.ofNullable(installationId);
    }
}
