package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.AtlassianConnectPerTenantEnvironmentConfiguration;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.stereotype.Component;

@Component("oauth2-jwt-impersonation")
public class OAuth2JwtClientRegistrationRepository implements ClientRegistrationRepository {

    static final AuthorizationGrantType JWT_BEARER_GRANT_TYPE = new AuthorizationGrantType("urn:ietf:params:oauth:grant-type:jwt-bearer");

    private final AtlassianConnectPerTenantEnvironmentConfiguration perTenantEnvironmentConfiguration;

    private final AtlassianHostRepository atlassianHostRepository;

    public OAuth2JwtClientRegistrationRepository(AtlassianHostRepository atlassianHostRepository, AtlassianConnectProperties properties) {
        this.atlassianHostRepository = atlassianHostRepository;
        this.perTenantEnvironmentConfiguration = new AtlassianConnectPerTenantEnvironmentConfiguration(properties);
    }

    @Override
    public ClientRegistration findByRegistrationId(String registrationId) {
        return atlassianHostRepository.findById(registrationId)
                .map(this::createClientRegistration)
                .orElse(null);
    }

    private ClientRegistration createClientRegistration(AtlassianHost host) {
        return ClientRegistration.withRegistrationId(host.getClientKey())
                .authorizationGrantType(JWT_BEARER_GRANT_TYPE)
                .clientId(host.getOauthClientId())
                .clientSecret(host.getSharedSecret())
                .tokenUri(perTenantEnvironmentConfiguration.getAuthorizationServerBaseUrl(host) + "/oauth2/token")
                .build();
    }
}
