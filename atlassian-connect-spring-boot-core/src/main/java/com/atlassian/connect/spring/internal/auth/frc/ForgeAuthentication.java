package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.ForgeInvocationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * ForgeAuthentication is an implementation of the Spring Security Authentication interface that is used to represent
 * the authenticated user in the Forge environment.
 * @see ForgeInvocationToken
 */
public class ForgeAuthentication implements Authentication {

    private final ForgeApiContext forgeApiContext;

    private AtlassianHostUser hostUser;

    public ForgeAuthentication(ForgeApiContext forgeApiContext, AtlassianHostUser hostUser) {
        this.forgeApiContext = forgeApiContext;
        this.hostUser = hostUser;
    }

    public ForgeAuthentication(ForgeApiContext forgeApiContext) {
        this.forgeApiContext = forgeApiContext;
        this.hostUser = null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public ForgeApiContext getDetails() {
        return forgeApiContext;
    }

    @Override
    public Object getPrincipal() {
        return hostUser;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return null;
    }
}

