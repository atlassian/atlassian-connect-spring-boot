package com.atlassian.connect.spring.internal.lifecycle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotNull;

/**
 * The HTTP POST body of Atlassian Connect add-on lifecycle events.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LifecycleEvent {

    public static final String CLIENT_CREDENTIALS = "oauth2";

    @NotNull
    public String eventType;

    @NotNull
    public String key;

    @NotNull
    public String clientKey;

    public String oauthClientId;

    public String sharedSecret;

    public String authentication; // "oauth2" | "jwt" | null (if oauth2 for h11n feature is not enabled)

    public String cloudId; // a unique identifier for a cloud instance, null if oauth2 for h11n feature is not enabled

    public String serverVersion;

    public String pluginsVersion;

    @NotNull
    public String baseUrl;

    public String displayUrl;

    public String displayUrlServicedeskHelpCenter;

    public String capabilitySet; // "capabilityStandard" | "capabilityAdvanced" | null (if app editions for licensed apps is not enabled)

    @NotNull
    public String productType;

    public String description;

    public String serviceEntitlementNumber;

    public String entitlementId;

    public String entitlementNumber;

    public String installationId;
}
