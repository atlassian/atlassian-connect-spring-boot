package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.SecurityConfigurer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * A configuration class responsible for registering a {@link JwtAuthenticationProvider} without interfering with
 * auto-configured Basic authentication support.
 */
@Configuration
public class JwtGlobalAuthenticationConfigurerAdapter extends GlobalAuthenticationConfigurerAdapter {

    private final AddonDescriptorLoader addonDescriptorLoader;

    private final AtlassianHostRepository hostRepository;

    private final SecurityProperties securityProperties;

    private final ObjectProvider<PasswordEncoder> passwordEncoder;

    public JwtGlobalAuthenticationConfigurerAdapter(AddonDescriptorLoader addonDescriptorLoader,
                                                    AtlassianHostRepository hostRepository,
                                                    SecurityProperties securityProperties,
                                                    ObjectProvider<PasswordEncoder> passwordEncoder) {
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.hostRepository = hostRepository;
        this.securityProperties = securityProperties;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void init(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(addonDescriptorLoader, hostRepository);
        authenticationManagerBuilder.apply(new JwtSecurityConfigurer<>(jwtAuthenticationProvider));

        UserDetailsServiceAutoConfiguration userDetailsServiceAutoConfiguration = new UserDetailsServiceAutoConfiguration();
        authenticationManagerBuilder.userDetailsService(userDetailsServiceAutoConfiguration.inMemoryUserDetailsManager(securityProperties, passwordEncoder));
    }

    private static class JwtSecurityConfigurer<T extends AuthenticationProvider> implements SecurityConfigurer<AuthenticationManager, AuthenticationManagerBuilder> {

        private final T authenticationProvider;

        public JwtSecurityConfigurer(T authenticationProvider) {
            this.authenticationProvider = authenticationProvider;
        }

        @Override
        public void init(AuthenticationManagerBuilder builder) {
            // do nothing
        }

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
            authenticationManagerBuilder.authenticationProvider(authenticationProvider);
        }
    }
}
