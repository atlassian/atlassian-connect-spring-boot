package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.UserAgentProvider;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Optional;

public class OAuth2HttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private final OAuth2JwtTokenService oAuth2JwtTokenService;
    private final AtlassianHostUser hostUser;
    private final AtlassianHostUriResolver atlassianHostUriResolver;

    public OAuth2HttpRequestInterceptor(AtlassianHostUser hostUser,
                                        OAuth2JwtTokenService oAuth2JwtTokenService,
                                        UserAgentProvider userAgentProvider,
                                        AtlassianHostUriResolver atlassianHostUriResolver) {
        super(userAgentProvider::getUserAgent);
        this.hostUser = hostUser;
        this.oAuth2JwtTokenService = oAuth2JwtTokenService;
        this.atlassianHostUriResolver = atlassianHostUriResolver;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = hostUser.getHost();
        assertRequestToHost(request, host);
        return Optional.of(host);
    }

    @Override
    protected URI wrapUri(HttpRequest request, AtlassianHost host) {
        URI uri = request.getURI();
        return uri.isAbsolute() ? uri : atlassianHostUriResolver.resolveToAbsoluteUriWithBase(uri, host.getBaseUrl());
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        String token = oAuth2JwtTokenService.getAccessToken(hostUser).getTokenValue();
        request.getHeaders().setBearerAuth(token);
        return request;
    }
}
