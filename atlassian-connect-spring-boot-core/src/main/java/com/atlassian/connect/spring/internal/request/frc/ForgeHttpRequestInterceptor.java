package com.atlassian.connect.spring.internal.request.frc;

import com.atlassian.connect.spring.ForgeSystemAccessToken;
import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.internal.auth.frc.ForgeInvocationTokenAuthenticationFilter;
import com.atlassian.connect.spring.internal.auth.frc.ForgeSecurityContextRetriever;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class ForgeHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(ForgeHttpRequestInterceptor.class);
    private final ForgeRemoteRequestAuthType provider;
    private final ForgeRemoteRequestType remote;
    private final ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository;
    private final AtlassianHostUriResolver hostUriResolver;
    private final Function<HttpRequest, Optional<ForgeApiContext>> invocationTokenSupplier;
    private final String forgeApiHost;
    private String installationId;
    @Value("${forge.system-access-token.expiration-leeway-ms:200}")
    private long systemAccessTokenExpirationLeewayMs;

    public ForgeHttpRequestInterceptor(ForgeRemoteRequestAuthType provider,
                                       ForgeRemoteRequestType remote,
                                       AtlassianHostUriResolver hostUriResolver,
                                       ForgeSecurityContextRetriever forgeContextRetriever,
                                       ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository,
                                       String forgeApiHost) {
        this.provider = provider;
        this.remote = remote;
        this.hostUriResolver = hostUriResolver;
        this.invocationTokenSupplier = request -> getForgeApiContext(forgeContextRetriever);
        this.forgeSystemAccessTokenRepository = forgeSystemAccessTokenRepository;
        this.forgeApiHost = forgeApiHost;
    }

    @Override
    @Nonnull
    public ClientHttpResponse intercept(@Nonnull HttpRequest request,
                                        @Nonnull byte[] body,
                                        @Nonnull ClientHttpRequestExecution execution) throws IOException {
        HttpRequest interceptedRequest = getForgeInvocationTokenForRequest(request)
            .map(token -> wrapRequest(request, token))
            .orElse(wrapRequest(request, installationId));
        return execution.execute(interceptedRequest, body);
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    protected Optional<ForgeApiContext> getForgeInvocationTokenForRequest(HttpRequest request) {
        return invocationTokenSupplier.apply(request);
    }

    private URI wrapUri(HttpRequest request, String apiBaseUrl) {
        final URI uri = request.getURI();
        final String baseUrl = getBaseUrl(apiBaseUrl);
        return uri.isAbsolute() ? uri : this.hostUriResolver.resolveToAbsoluteUriWithBase(uri, baseUrl);
    }

    private String getBaseUrl(String apiBaseUrl) {
        if (this.remote == ForgeRemoteRequestType.CONFLUENCE) {
            return apiBaseUrl + "/wiki";
        }
        if (this.remote == ForgeRemoteRequestType.OTHER) {
            return "https://" + forgeApiHost;
        }
        return apiBaseUrl;
    }

    private HttpRequest injectHeader(HttpRequest request, ForgeApiContext apiContext) {
        if (!request.getURI().getHost().equals(forgeApiHost)) {
            return request;
        }
        if (provider == ForgeRemoteRequestAuthType.USER) {
            apiContext.getUserToken().ifPresent(token -> request.getHeaders().setBearerAuth(token));
        } else if (provider == ForgeRemoteRequestAuthType.APP) {
            apiContext.getAppToken().ifPresent(token -> request.getHeaders().setBearerAuth(token));
        }
        return request;
    }

    private HttpRequest injectHeader(HttpRequest request, String accessToken) {
        if (!request.getURI().getHost().equals(forgeApiHost)) {
            return request;
        }
        if (provider == ForgeRemoteRequestAuthType.APP) {
            request.getHeaders().setBearerAuth(accessToken);
        }
        return request;
    }

    private Optional<ForgeApiContext> getForgeApiContext(ForgeSecurityContextRetriever forgeContextRetriever) {
        return forgeContextRetriever.getForgeApiContext();
    }

    private HttpRequest wrapRequest(HttpRequest request, ForgeApiContext apiContext) {
        URI uri = wrapUri(request, apiContext.getForgeInvocationToken().getApp().getApiBaseUrl());

        HttpRequestWrapper requestWrapper = new ForgeRequestWrapper(request, uri);
        return injectHeader(requestWrapper, apiContext);
    }

    /**
     * While the access token can expire, there's no option yet for the app to refresh the access token proactively
     */
    private HttpRequest wrapRequest(HttpRequest request, String installationId) {
        if (Objects.isNull(installationId)) {
            return request;
        }
        Optional<ForgeSystemAccessToken> accessToken = findSystemAccessToken(installationId);
        if (accessToken.isEmpty()) {
            log.warn("""
                    There is no unexpired access token found for {}.
                    Consider having a scheduled-trigger to refresh the access token periodically.
                    """,
                installationId);
            return request;
        }
        URI uri = wrapUri(request, accessToken.get().getApiBaseUrl());

        HttpRequestWrapper requestWrapper = new ForgeRequestWrapper(request, uri);
        return injectHeader(requestWrapper, accessToken.get().getAccessToken());
    }

    private Optional<ForgeSystemAccessToken> findSystemAccessToken(String installationId) {
        Instant criteria = Instant.now().plusMillis(systemAccessTokenExpirationLeewayMs);
        log.debug("Searching a system access token for installation {}", installationId);
        return forgeSystemAccessTokenRepository.findByInstallationIdAndExpirationTimeAfter(
            installationId,
            Timestamp.from(criteria)
        );
    }

    private static class ForgeRequestWrapper extends HttpRequestWrapper {
        private URI uri;

        public ForgeRequestWrapper(HttpRequest request, URI uri) {
            super(request);
            this.uri = uri;
        }

        @Override
        @Nonnull
        public URI getURI() {
            return uri;
        }
    }
}
