package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.system.JavaVersion;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class UserAgentProvider {

    private static final String USER_AGENT_PRODUCT = "atlassian-connect-spring-boot";

    private final String atlassianConnectClientVersion;

    private final AddonDescriptorLoader addonDescriptorLoader;

    public UserAgentProvider(
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion,
            AddonDescriptorLoader addonDescriptorLoader) {
        this.atlassianConnectClientVersion = atlassianConnectClientVersion;
        this.addonDescriptorLoader = addonDescriptorLoader;
    }

    public String getUserAgent() {
        return getUserAgent(this.atlassianConnectClientVersion, SpringBootVersion.getVersion(), SpringVersion.getVersion(), JavaVersion.getJavaVersion(), addonDescriptorLoader.getDescriptor());
    }

    public String getUserAgent(String atlassianConnectClientVersion, String springBootVersion, String springVersion, JavaVersion javaVersion, AddonDescriptor addonDescriptor) {
        return Stream.of(
                        Optional.of(getUserAgentProduct(USER_AGENT_PRODUCT, atlassianConnectClientVersion)),
                        Optional.ofNullable(springBootVersion).map(version -> getUserAgentProduct("SpringBoot", version)),
                        Optional.ofNullable(springVersion).map(version -> getUserAgentProduct("Spring", version)),
                        Optional.ofNullable(javaVersion).map(version -> getUserAgentProduct("Java", version.toString())),
                        Optional.ofNullable(addonDescriptor.getKey())
                ).filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.joining(" "));
    }

    private String getUserAgentProduct(String product, String version) {
        return String.format("%s/%s", product, version);
    }
}
