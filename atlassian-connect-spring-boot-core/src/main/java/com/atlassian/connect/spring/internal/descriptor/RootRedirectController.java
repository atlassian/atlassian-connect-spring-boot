package com.atlassian.connect.spring.internal.descriptor;

import com.atlassian.connect.spring.IgnoreJwt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

import static com.atlassian.connect.spring.internal.descriptor.AddonDescriptorController.DESCRIPTOR_REQUEST_PATH;

/**
 * A controller that redirects the root (<code>/</code>) to the Atlassian Connect add-on descriptor (<code>atlassian-connect.json</code>).
 */
@IgnoreJwt
@Controller
@ConditionalOnProperty(prefix = "atlassian.connect", name = "redirect-root-to-descriptor", matchIfMissing = true)
public class RootRedirectController {

    private static final Logger log = LoggerFactory.getLogger(RootRedirectController.class);

    @GetMapping("/")
    public RedirectView getRoot() {
        log.debug("Redirecting / to {}", DESCRIPTOR_REQUEST_PATH);
        RedirectView redirectView = new RedirectView(DESCRIPTOR_REQUEST_PATH, true);
        redirectView.setHttp10Compatible(false);
        redirectView.setStatusCode(HttpStatus.FOUND);
        return redirectView;
    }
}
