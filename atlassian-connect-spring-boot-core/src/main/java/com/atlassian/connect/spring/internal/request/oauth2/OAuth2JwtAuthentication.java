package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class OAuth2JwtAuthentication implements Authentication {

    private final AtlassianHostUser hostUser;

    public OAuth2JwtAuthentication(AtlassianHostUser hostUser) {
        this.hostUser = hostUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptySet();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return hostUser;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(hostUser.getHost().getClientKey());
        hostUser.getUserAccountId().ifPresent(userAccountId -> nameBuilder.append(String.format(" (%s)", userAccountId)));

        return nameBuilder.toString();
    }
}
