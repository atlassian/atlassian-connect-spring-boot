package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.ForgeInvocationToken;
import com.atlassian.connect.spring.ForgeSystemAccessToken;
import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;


public class ForgeInvocationTokenAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(ForgeInvocationTokenAuthenticationFilter.class);

    private final ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository;
    private final ForgeInvocationTokenValidator forgeInvocationTokenValidator;
    private final LifecycleURLHelper lifecycleURLHelper;
    private final String X_FORGE_OAUTH_SYSTEM = "x-forge-oauth-system";
    private final String X_FORGE_OAUTH_USER = "x-forge-oauth-user";
    public ForgeInvocationTokenAuthenticationFilter(
        ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository,
        ForgeInvocationTokenValidator forgeInvocationTokenValidator,
        LifecycleURLHelper lifecycleURLHelper) {
        this.forgeSystemAccessTokenRepository = forgeSystemAccessTokenRepository;
        this.forgeInvocationTokenValidator = forgeInvocationTokenValidator;
        this.lifecycleURLHelper = lifecycleURLHelper;
    }

    @Override
    protected void doFilterInternal(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response, @Nonnull FilterChain filterChain) throws ServletException, IOException {
        if (lifecycleURLHelper.isRequestToLifecycleURL(request)) {
            log.debug("Ignoring lifecycle endpoint");
            filterChain.doFilter(request, response);
            return;
        }
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        Optional<ForgeInvocationToken> optionalForgeInvocationToken = getForgeInvocationTokenIfPresent(authHeader);
        if (optionalForgeInvocationToken.isPresent()) {
            Optional<String> appToken = Optional.ofNullable(request.getHeader(X_FORGE_OAUTH_SYSTEM));
            Optional<String> userToken = Optional.ofNullable(request.getHeader(X_FORGE_OAUTH_USER));
            Authentication authenticationResult = new ForgeAuthentication(new ForgeApiContext(optionalForgeInvocationToken.get(), userToken, appToken));
            appToken.ifPresent(token -> refreshSystemAccessToken(optionalForgeInvocationToken.get(), token));
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        }
        filterChain.doFilter(request, response);
    }

    private void refreshSystemAccessToken(ForgeInvocationToken fit, String appToken) {
        ForgeSystemAccessToken entity = new ForgeSystemAccessToken();
        try {
            entity.setInstallationId(fit.getApp().getInstallationId());
            entity.setApiBaseUrl(fit.getApp().getApiBaseUrl());
            entity.setAccessToken(appToken);
            entity.parseAndSetClaims(appToken);
        } catch (ParseException e) {
            // We want to fail silently as token refresh isn't critical
            log.error("Error parsing an app token", e);
        }
        log.debug("Storing a system access token: installationId={}, isFulfilled={}",
            entity.getInstallationId(),
            ForgeSystemAccessToken.isFulfilled(entity)
        );
        try {
            if (ForgeSystemAccessToken.isFulfilled(entity)) {
                forgeSystemAccessTokenRepository.save(entity);
            }
        } catch (Exception e) {
            // We want to fail silently as token refresh isn't critical
            log.error("Error storing a system access token", e);
        }
    }

    private Optional<ForgeInvocationToken> getForgeInvocationTokenIfPresent(String authHeader) {
        if (authHeader == null || !authHeader.toLowerCase().startsWith(OAuth2AccessToken.TokenType.BEARER.getValue().toLowerCase())) {
            return Optional.empty();
        }
        try {
            ForgeInvocationToken forgeInvocationToken = forgeInvocationTokenValidator.validateForgeInvocationToken(authHeader);
            return Optional.of(forgeInvocationToken);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}




