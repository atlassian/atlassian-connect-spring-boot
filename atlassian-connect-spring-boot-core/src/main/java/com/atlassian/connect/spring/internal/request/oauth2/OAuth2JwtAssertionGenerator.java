package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.jwt.InvalidKeyException;
import com.atlassian.connect.spring.internal.jwt.JwtJsonBuilder;
import com.atlassian.connect.spring.internal.jwt.JwtWriter;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
public class OAuth2JwtAssertionGenerator {

    private static final Logger log = LoggerFactory.getLogger(OAuth2JwtAssertionGenerator.class);

    public String getAssertionString(AtlassianHostUser hostUser, String tokenUri) {
        long iat = System.currentTimeMillis() / 1000;
        Optional<String> userAccountId = hostUser.getUserAccountId();

        if (userAccountId.isEmpty()) {
            // If neither are present.
            throw new IllegalArgumentException("The userAccountId must be provided.");
        }

        String authorizationServerUrl = UriComponentsBuilder.fromHttpUrl(tokenUri).replacePath(null).build().toUriString();
        String json = new JwtJsonBuilder()
                .issuedAt(iat)
                .expirationTime(iat + 60L) // Assertions have a maximum life span of 60 seconds
                .issuer("urn:atlassian:connect:clientid:" + hostUser.getHost().getOauthClientId())
                .audience(authorizationServerUrl)
                .claim("tnt", hostUser.getHost().getBaseUrl())
                .subject("urn:atlassian:connect:useraccountid:" + userAccountId.get())
                .build();

        log.debug("Created OAuth 2.0 JWT assertion: {}", json);
        return createJwtWriter(hostUser.getHost().getSharedSecret()).jsonToJwt(json);
    }

    private JwtWriter createJwtWriter(String secret) {
        try {
            return new JwtWriter(JWSAlgorithm.HS256, new MACSigner(secret));
        } catch (KeyLengthException e) {
            throw new InvalidKeyException(e);
        }
    }
}
