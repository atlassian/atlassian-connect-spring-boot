package com.atlassian.connect.spring.internal.descriptor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * An Atlassian Connect add-on descriptor (<code>atlassian-connect.json</code>).
 */
public class AddonDescriptor {

    private String key;

    private String baseUrl;

    private Map<String, String> regionBaseUrls;

    private Authentication authentication;

    private Lifecycle lifecycle;

    private ApiMigrations apiMigrations;

    public String getKey() {
        return key;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    @JsonIgnore
    public Stream<URI> getBaseUrls() {
        Stream.Builder<URI> stream = Stream.builder();
        stream.add(URI.create(baseUrl));
        if (regionBaseUrls != null) {
            regionBaseUrls.forEach((key, value) -> stream.add(URI.create(value)));
        }
        return stream.build();
    }

    @JsonIgnore
    public String getAuthenticationType() {
        return Optional.ofNullable(authentication).map(Authentication::getType).orElse(null);
    }

    @JsonIgnore
    public String getInstalledLifecycleUrl() {
        return getLifecycleUrl(Lifecycle::getInstalled);
    }

    @JsonIgnore
    public String getUninstalledLifecycleUrl() {
        return getLifecycleUrl(Lifecycle::getUninstalled);
    }

    @JsonIgnore
    public String getEnabledLifecycleUrl() {
        return getLifecycleUrl(Lifecycle::getEnabled);
    }

    @JsonIgnore
    public String getDisabledLifecycleUrl() {
        return getLifecycleUrl(Lifecycle::getDisabled);
    }

    @JsonIgnore
    private String getLifecycleUrl(Function<Lifecycle, String> urlRetriever) {
        return Optional.ofNullable(lifecycle).map(urlRetriever).orElse(null);
    }

    private static class Authentication {

        private String type;

        public String getType() {
            return type;
        }
    }

    private static class Lifecycle {

        private String installed;

        private String uninstalled;

        private String enabled;

        private String disabled;

        public String getInstalled() {
            return installed;
        }

        public String getUninstalled() {
            return uninstalled;
        }

        public String getEnabled() {
            return enabled;
        }

        public String getDisabled() {
            return disabled;
        }
    }

    private static class ApiMigrations {
        @JsonProperty("context-qsh")
        private Boolean contextQsh;

        private ApiMigrations() {}

        public Boolean getContextQsh() {
            return contextQsh;
        }
    }
}
