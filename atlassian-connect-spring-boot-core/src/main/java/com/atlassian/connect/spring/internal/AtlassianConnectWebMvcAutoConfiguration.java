package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.internal.auth.RequireAuthenticationHandlerInterceptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * A Spring Web MVC configuration class, providing custom functionality for Atlassian Connect.
 */
@Configuration
@ConditionalOnResource(resources = AddonDescriptorLoader.DESCRIPTOR_RESOURCE_PATH)
public class AtlassianConnectWebMvcAutoConfiguration implements WebMvcConfigurer {

    private final AtlassianConnectProperties atlassianConnectProperties;

    private final RequireAuthenticationHandlerInterceptor requireAuthenticationHandlerInterceptor;

    public AtlassianConnectWebMvcAutoConfiguration(AtlassianConnectProperties atlassianConnectProperties,
                                                   RequireAuthenticationHandlerInterceptor requireAuthenticationHandlerInterceptor) {
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.requireAuthenticationHandlerInterceptor = requireAuthenticationHandlerInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requireAuthenticationHandlerInterceptor)
                .addPathPatterns(atlassianConnectProperties.getRequireAuthIncludePaths())
                .excludePathPatterns(atlassianConnectProperties.getRequireAuthExcludePaths());
    }
}
