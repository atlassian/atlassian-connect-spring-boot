package com.atlassian.connect.spring.internal.descriptor;

import com.atlassian.connect.spring.IgnoreJwt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller that serves the Atlassian Connect add-on descriptor (<code>atlassian-connect.json</code>).
 */
@IgnoreJwt
@RestController
public class AddonDescriptorController {

    private static final Logger log = LoggerFactory.getLogger(AddonDescriptorController.class);

    static final String DESCRIPTOR_REQUEST_PATH = "/" + AddonDescriptorLoader.DESCRIPTOR_FILENAME;

    private final AddonDescriptorLoader addonDescriptorLoader;

    public AddonDescriptorController(AddonDescriptorLoader addonDescriptorLoader) {
        this.addonDescriptorLoader = addonDescriptorLoader;
    }

    @GetMapping(value = DESCRIPTOR_REQUEST_PATH, produces = "application/json")
    public String getDescriptor() {
        String rawDescriptor = addonDescriptorLoader.getRawDescriptor();
        log.debug("Serving add-on descriptor: {}", rawDescriptor);
        return rawDescriptor;
    }
}
