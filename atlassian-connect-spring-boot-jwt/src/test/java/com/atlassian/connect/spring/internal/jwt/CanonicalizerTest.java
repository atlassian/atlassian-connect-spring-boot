package com.atlassian.connect.spring.internal.jwt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

/**
 * Tests aspects of the http request canonicalizer
 */
@ExtendWith(MockitoExtension.class)
class CanonicalizerTest {
    @Mock
    private CanonicalHttpRequest request;

    @BeforeEach
    public void setUp() {
        // Some servlet containers may return an array with a null value for a parameter
        Map<String, String[]> mapWithNulls = new HashMap<>();
        mapWithNulls.put("p1", new String[]{"value"});
        mapWithNulls.put("p2", new String[]{null});
        mapWithNulls.put("p3", new String[]{""});
        mapWithNulls.put("p4", new String[]{"other"});
        mapWithNulls.put("jwt", new String[]{"fake-jwt"});
        when(request.getParameterMap()).thenReturn(mapWithNulls);

        when(request.getRelativePath()).thenReturn("/path");
        when(request.getMethod()).thenReturn("GET");
    }

    /**
     * Test that the toVerboseString method can handle a parameter map with null entries
     */
    @Test
    void testVerboseStringWithNullParameterMap() {
        try {
            CanonicalRequestUtil.toVerboseString(request);
        } catch (Exception e) {
            fail("VerboseString threw unexpected exception: " + e.getClass().getCanonicalName());
        }
    }

    /**
     * Test that canonicalizing the request can handle null entries in the parameter map
     */
    @Test
    void testCanonicaliseQueryParameters() {
        String result = HttpRequestCanonicalizer.canonicalize(request);
        assertEquals("GET&/path&p1=value&p2=&p3=&p4=other", result, "Canonicalised request incorrect");
    }
}
