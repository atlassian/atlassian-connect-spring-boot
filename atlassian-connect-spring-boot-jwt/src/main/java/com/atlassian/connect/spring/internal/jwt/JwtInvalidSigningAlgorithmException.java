package com.atlassian.connect.spring.internal.jwt;

/**
 * An authentication exception thrown when processing a JSON Web Token is signed with an algorithm not supported by
 * the authentication provider that tried to process it
 */

public class JwtInvalidSigningAlgorithmException extends JwtVerificationException {

    public JwtInvalidSigningAlgorithmException(String message) {
        super(message);
    }

    public JwtInvalidSigningAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }
}
