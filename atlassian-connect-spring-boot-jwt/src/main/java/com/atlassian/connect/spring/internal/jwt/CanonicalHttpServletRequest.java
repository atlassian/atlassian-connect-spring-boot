package com.atlassian.connect.spring.internal.jwt;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

public class CanonicalHttpServletRequest implements CanonicalHttpRequest {
    
    private final HttpServletRequest request;

    public CanonicalHttpServletRequest(final HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public String getRelativePath() {
        return CanonicalRequestUtil.getRelativePath(request.getRequestURI(), request.getContextPath());
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return request.getParameterMap();
    }
}
