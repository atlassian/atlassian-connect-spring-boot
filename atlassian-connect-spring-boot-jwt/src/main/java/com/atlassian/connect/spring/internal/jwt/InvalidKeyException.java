package com.atlassian.connect.spring.internal.jwt;

/**
 * Indicates the supplied key is not valid.
 * The source throwable will contain the underlying failure and error message.
 */
public class InvalidKeyException extends RuntimeException {

    public InvalidKeyException() {
    }

    public InvalidKeyException(String message) {
        super(message);
    }

    public InvalidKeyException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidKeyException(Throwable cause) {
        super(cause);
    }

    public InvalidKeyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
